# Power
"Power" takes place in an alternate reality where people actually care about climate change.

## Aim of the Game
You are working at the public utility company of a small town,
and your main task is to provide the town with enough power for the citizens.
But beware! The citizens want you to emit as little CO₂ as possible.
The more CO₂ you emit, the lower your reputation becomes and the less money you will get.
If you do not have enough money, you can not keep up with the growing power demand
and will be replaced by someone, who, contrary to you, does a decent job.

## Getting Started
Before playing, it is recommended to read the tutorial.
It can be found under `docs/tutorial.md.`

## Credits
- https://github.com/bevyengine/bevy
- https://github.com/mvlabat/bevy_egui
- https://github.com/aevyrie/bevy_mod_raycast
- https://github.com/NiklasEi/bevy_asset_loader
- https://github.com/becheran/grid
- https://github.com/Peternator7/strum
- https://github.com/johanhelsing/noisy_bevy
- https://github.com/lampsitter/egui_commonmark
- https://github.com/BlackPhlox/bevy_dolly
- https://github.com/mamrhein/quantities.rs
- https://github.com/JonahPlusPlus/bevy_atmosphere
- https://github.com/rustwasm/console_error_panic_hook

## License

Power is dual-licensed under MIT and Apache-2.0.
