use quantities::currency::{
    Currency, BILLION_EURO, EURO, MILLION_EURO, THOUSAND_EURO, TRILLION_EURO,
};
use quantities::energy::{Energy, GIGAWATT_HOUR, KILOWATT_HOUR, MEGAWATT_HOUR, WATT_HOUR};
use quantities::mass::{Mass, GIGATONNE, KILOTONNE, MEGATONNE, TONNE};
use quantities::mass_flow_rate::{
    MassFlowRate, GIGATONNE_PER_MONTH, KILOTONNE_PER_MONTH, MEGATONNE_PER_MONTH, TONNE_PER_MONTH,
};
use quantities::power::{Power, GIGAWATT, KILOWATT, MEGAWATT, TERAWATT, WATT};
use quantities::{HasRefUnit, Quantity};

use crate::core::resources::{Reputation, PERCENT};

pub mod bottom_panel;
pub mod end_screen;
pub mod info_windows;
pub mod main_menu;
pub mod pause_menu;
pub mod top_panel;
pub mod tutorial;
pub mod ui_plugin;

pub trait QuantityExt: Quantity {
    const DEFAULT_UNIT: <Self as Quantity>::UnitType;

    fn to_formatted_string(&self) -> String;
}

impl QuantityExt for Currency {
    const DEFAULT_UNIT: <Self as Quantity>::UnitType = Self::REF_UNIT;

    fn to_formatted_string(&self) -> String {
        let unit = match self.equiv_amount(EURO) {
            x if x.abs() >= 1e12 => TRILLION_EURO,
            x if x.abs() >= 1e9 => BILLION_EURO,
            x if x.abs() >= 1e6 => MILLION_EURO,
            x if x.abs() >= 1e3 => THOUSAND_EURO,
            _ => EURO,
        };

        format!("{:.3}{}", self.equiv_amount(unit), unit)
    }
}

impl QuantityExt for Energy {
    const DEFAULT_UNIT: <Self as Quantity>::UnitType = WATT_HOUR;

    fn to_formatted_string(&self) -> String {
        let unit = match self.equiv_amount(WATT_HOUR) {
            x if x.abs() >= 1e9 => GIGAWATT_HOUR,
            x if x.abs() >= 1e6 => MEGAWATT_HOUR,
            x if x.abs() >= 1e3 => KILOWATT_HOUR,
            _ => WATT_HOUR,
        };

        format!("{:.3} {}", self.equiv_amount(unit), unit)
    }
}

impl QuantityExt for Mass {
    const DEFAULT_UNIT: <Self as Quantity>::UnitType = Self::REF_UNIT;

    fn to_formatted_string(&self) -> String {
        let unit = match self.equiv_amount(TONNE) {
            x if x.abs() >= 1e9 => GIGATONNE,
            x if x.abs() >= 1e6 => MEGATONNE,
            x if x.abs() >= 1e3 => KILOTONNE,
            _ => TONNE,
        };

        format!("{:.3} {}", self.equiv_amount(unit), unit)
    }
}

impl QuantityExt for MassFlowRate {
    const DEFAULT_UNIT: <Self as Quantity>::UnitType = Self::REF_UNIT;

    fn to_formatted_string(&self) -> String {
        let unit = match self.equiv_amount(TONNE_PER_MONTH) {
            x if x.abs() >= 1e9 => GIGATONNE_PER_MONTH,
            x if x.abs() >= 1e6 => MEGATONNE_PER_MONTH,
            x if x.abs() >= 1e3 => KILOTONNE_PER_MONTH,
            _ => TONNE_PER_MONTH,
        };

        format!("{:.3} {}", self.equiv_amount(unit), unit)
    }
}

impl QuantityExt for Reputation {
    const DEFAULT_UNIT: <Self as Quantity>::UnitType = PERCENT;

    fn to_formatted_string(&self) -> String {
        format!("{:.3}{}", self.equiv_amount(PERCENT), PERCENT)
    }
}

impl QuantityExt for Power {
    const DEFAULT_UNIT: <Self as Quantity>::UnitType = Self::REF_UNIT;

    fn to_formatted_string(&self) -> String {
        let unit = match self.equiv_amount(WATT) {
            x if x.abs() >= 1e12 => TERAWATT,
            x if x.abs() >= 1e9 => GIGAWATT,
            x if x.abs() >= 1e6 => MEGAWATT,
            x if x.abs() >= 1e3 => KILOWATT,
            _ => WATT,
        };

        format!("{:.3} {}", self.equiv_amount(unit), unit)
    }
}
