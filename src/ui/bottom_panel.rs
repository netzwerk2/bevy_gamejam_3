use bevy::prelude::*;
use bevy_egui::egui::{Color32, ProgressBar, TopBottomPanel};
use bevy_egui::EguiContexts;
use quantities::duration::{MONTH, SECOND};
use quantities::HasRefUnit;
use strum::IntoEnumIterator;

use crate::core::months::{LastMonthStatistics, LossTimer, MonthTimer};
use crate::core::states::GameState;
use crate::core::time_scale::TimeScale;
use crate::ui::info_windows::{InfoWindow, WindowsOpen};
use crate::ui::ui_plugin::nice_button;

pub struct BottomPanelPlugin;

impl Plugin for BottomPanelPlugin {
    fn build(&self, app: &mut App) {
        app.add_system(bottom_panel.in_set(OnUpdate(GameState::InGame)))
            .add_system(bottom_panel.in_set(OnUpdate(GameState::Paused)));
    }
}

fn bottom_panel(
    mut contexts: EguiContexts,
    mut windows_open: ResMut<WindowsOpen>,
    month_timer: Res<MonthTimer>,
    loss_timer: Res<LossTimer>,
    mut time_scale: ResMut<TimeScale>,
    last_month: Res<LastMonthStatistics>,
) {
    let ctx = contexts.ctx_mut();
    TopBottomPanel::bottom("window_toggling_panel")
        .resizable(false)
        .exact_height(40.0)
        .show(ctx, |ui| {
            ui.add_space(10.0);
            ui.horizontal(|ui| {
                for info_window in InfoWindow::iter() {
                    if ui
                        .add(nice_button(info_window.to_string()))
                        .on_hover_text(info_window.tool_tip())
                        .clicked()
                    {
                        info_window.toggle(&mut windows_open)
                    }
                }

                ui.separator();

                for time_scale_mode in TimeScale::iter() {
                    let tooltip = time_scale_mode.button_tooltip();

                    let enabled = match time_scale_mode {
                        TimeScale::Paused(_) => !matches!(*time_scale, TimeScale::Paused(_)),
                        _ => time_scale_mode != *time_scale,
                    };

                    if ui
                        .add_enabled(enabled, nice_button(time_scale_mode.to_string()))
                        .on_hover_text(tooltip.clone())
                        .on_disabled_hover_text(tooltip)
                        .clicked()
                    {
                        *time_scale = time_scale_mode
                    };
                }

                ui.separator();
                let month_progress =
                    month_timer.elapsed().as_secs_f32() / (1.0 * MONTH).equiv_amount(SECOND) as f32;

                let mut month_progress_size = ui.available_size();
                month_progress_size.x *= 0.75;
                ui.add_sized(month_progress_size, ProgressBar::new(month_progress))
                    .on_hover_text(format!(
                        "Progress of the month {}.",
                        last_month.month_count + 1
                    ));
                ui.separator();

                let loss_progress =
                    loss_timer.elapsed().as_secs_f32() / loss_timer.duration().as_secs_f32();

                ui.add(
                    ProgressBar::new(loss_progress).fill(if loss_progress > 0.001 {
                        Color32::RED
                    } else {
                        Color32::from_gray(10)
                    }),
                )
                .on_hover_text("How close you are to losing. More Red = Moar Bad = Less Stonks");
            })
        });
}
