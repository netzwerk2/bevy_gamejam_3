use bevy::prelude::*;
use bevy_egui::egui::Context;
use bevy_egui::{egui, EguiContexts};
use strum::IntoEnumIterator;

use crate::assets::BuildingThumbnails;
use crate::core::build_selection::BuildSelection;
use crate::core::buildings::Building;
use crate::core::research::ResearchTree;
use crate::core::resources::Resources;
use crate::core::shipping_options::ShippingOption;
use crate::ui::info_windows::{
    update_window_hovering, HoveringAboveWindow, LastCursorPosition, WindowsOpen,
};
use crate::ui::ui_plugin::image_button;

#[derive(Default, Resource)]
pub struct BuildingSelectionWindow;

impl BuildingSelectionWindow {
    pub fn display(
        &mut self,
        window: &Window,
        last_cursor_position: &LastCursorPosition,
        research_tree: &ResearchTree,
        resources: &Resources,
        shipping_option: &ShippingOption,
        building_thumbnails: &BuildingThumbnails,
        hovering_above_window: &mut HoveringAboveWindow,
        windows_open: &mut WindowsOpen,
        mut selected_building: ResMut<BuildSelection>,
        ctx: &Context,
    ) {
        let enabled = windows_open.buildings_enabled;

        let response = egui::Window::new("Select Building")
            .open(&mut windows_open.building_selection_window)
            .resizable(false)
            .show(ctx, |ui| {
                ui.set_enabled(enabled);
                ui.vertical_centered_justified(|ui| {
                    for building_row in Building::iter()
                        .filter(|b| research_tree.can_build(*b))
                        .collect::<Vec<Building>>()
                        .chunks(3)
                    {
                        ui.horizontal(|ui| {
                            for building in building_row {
                                let enabled = resources.money >= building.price(shipping_option);
                                if ui
                                    .add_enabled(
                                        enabled,
                                        image_button(
                                            building_thumbnails.get_id_by_building(*building),
                                            Vec2::new(64.0, 64.0),
                                        ),
                                    )
                                    .on_hover_text(building.tool_tip(shipping_option))
                                    .on_disabled_hover_text(building.tool_tip(shipping_option))
                                    .clicked()
                                {
                                    *selected_building = BuildSelection::Building(*building);
                                }
                            }
                        });
                    }
                });
            });

        update_window_hovering(
            response,
            last_cursor_position,
            window,
            hovering_above_window,
        );
    }
}

pub fn building_selection_window(
    window: Query<&Window>,
    last_cursor_position: Res<LastCursorPosition>,
    research_tree: Res<ResearchTree>,
    resources: Res<Resources>,
    shipping_option: Res<ShippingOption>,
    building_thumbnails: Res<BuildingThumbnails>,
    mut hovering_above_window: ResMut<HoveringAboveWindow>,
    mut windows_open: ResMut<WindowsOpen>,
    selected_building: ResMut<BuildSelection>,
    mut contexts: EguiContexts,
    mut building_selection_window: ResMut<BuildingSelectionWindow>,
) {
    building_selection_window.display(
        window.single(),
        &last_cursor_position,
        &research_tree,
        &resources,
        &shipping_option,
        &building_thumbnails,
        &mut hovering_above_window,
        &mut windows_open,
        selected_building,
        contexts.ctx_mut(),
    );
}
