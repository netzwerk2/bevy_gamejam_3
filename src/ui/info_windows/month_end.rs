use bevy::prelude::*;
use bevy_egui::egui::{Context, Ui};
use bevy_egui::{egui, EguiContexts};

use crate::core::months::LastMonthStatistics;
use crate::core::states::GameState;
use crate::ui::info_windows::{update_window_hovering, HoveringAboveWindow, LastCursorPosition};
use crate::ui::ui_plugin::nice_button;
use crate::ui::QuantityExt;

#[derive(Default, Resource)]
pub struct MonthEndWindow;

impl MonthEndWindow {
    pub fn display(
        &mut self,
        window: &Window,
        last_cursor_position: &LastCursorPosition,
        last_month_statistics: &LastMonthStatistics,
        hovering_above_window: &mut HoveringAboveWindow,
        next_state: &mut NextState<GameState>,
        ctx: &Context,
    ) {
        let response = egui::Window::new(format!(
            "Month {} Ended!",
            last_month_statistics.month_count
        ))
        .resizable(false)
        .show(ctx, |ui| {
            Self::month_end_info_grid(ui, last_month_statistics);
            ui.vertical_centered_justified(|ui| {
                if ui.add(nice_button("Okay")).clicked() {
                    next_state.set(GameState::InGame);
                };
            });
        });

        update_window_hovering(
            response,
            last_cursor_position,
            window,
            hovering_above_window,
        );
    }
    pub fn month_end_info_grid(ui: &mut Ui, last_month_statistics: &LastMonthStatistics) {
        egui::Grid::new("month_end_grid").show(ui, |ui| {
            ui.label("Money earned");
            ui.label(last_month_statistics.money_earned.to_formatted_string());
            ui.end_row();

            ui.label("Reputation Earned");
            ui.label(
                last_month_statistics
                    .reputation_change_percent
                    .to_formatted_string(),
            );
            ui.end_row();
        });
    }
}

pub fn month_end_window(
    window: Query<&Window>,
    last_cursor_position: Res<LastCursorPosition>,
    last_month_statistics: Res<LastMonthStatistics>,
    mut hovering_above_window: ResMut<HoveringAboveWindow>,
    mut next_state: ResMut<NextState<GameState>>,
    mut contexts: EguiContexts,
    mut month_end_window: ResMut<MonthEndWindow>,
) {
    month_end_window.display(
        window.single(),
        &last_cursor_position,
        &last_month_statistics,
        &mut hovering_above_window,
        &mut next_state,
        contexts.ctx_mut(),
    )
}
