use std::fmt::{Display, Formatter};

use bevy::prelude::*;
use bevy_egui::egui::{InnerResponse, Pos2};
use strum_macros::EnumIter;

use crate::core::states::GameState;
use crate::ui::info_windows::building_selection::{
    building_selection_window, BuildingSelectionWindow,
};
use crate::ui::info_windows::current_building::{current_building_window, CurrentBuildingWindow};
use crate::ui::info_windows::month_end::{month_end_window, MonthEndWindow};
use crate::ui::info_windows::plots::{plots_window, PlotsWindow};
use crate::ui::info_windows::research::{research_window, ResearchWindow};
use crate::ui::info_windows::shipping::{shipping_window, ShippingWindow};
use crate::ui::ui_plugin::UiSystemSet;

pub mod building_selection;
pub mod current_building;
pub mod month_end;
pub mod plots;
pub mod research;
pub mod shipping;

#[derive(Default, Resource)]
pub struct WindowsOpen {
    pub building_selection_window: bool,
    pub buildings_enabled: bool,
    pub last_month_window: bool,
    pub plots_window: bool,
    pub research_window: bool,
    pub shipping_window: bool,
}

pub struct InfoWindowPlugin;

impl Plugin for InfoWindowPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<WindowsOpen>()
            .init_resource::<HoveringAboveWindow>()
            .init_resource::<LastCursorPosition>()
            .init_resource::<BuildingSelectionWindow>()
            .init_resource::<MonthEndWindow>()
            .init_resource::<PlotsWindow>()
            .init_resource::<ResearchWindow>()
            .init_resource::<ShippingWindow>()
            .init_resource::<CurrentBuildingWindow>()
            .add_system(month_end_window.in_set(OnUpdate(GameState::MonthEnd)))
            .add_system(close_all.in_schedule(OnEnter(GameState::Menu)))
            .add_system(close_all.in_schedule(OnEnter(GameState::GameLost)))
            .add_system(set_buildings_enabled.in_schedule(OnEnter(GameState::InGame)))
            .add_system(set_buildings_disabled.in_schedule(OnExit(GameState::InGame)))
            .add_system(
                building_selection_window
                    .run_if(|windows_open: Res<WindowsOpen>| windows_open.building_selection_window)
                    .in_set(UiSystemSet::InGame),
            )
            .add_system(
                plots_window
                    .run_if(|windows_open: Res<WindowsOpen>| windows_open.plots_window)
                    .in_set(UiSystemSet::InGame),
            )
            .add_system(
                research_window
                    .run_if(|windows_open: Res<WindowsOpen>| windows_open.research_window)
                    .in_set(UiSystemSet::InGame),
            )
            .add_system(
                shipping_window
                    .run_if(|windows_open: Res<WindowsOpen>| windows_open.shipping_window)
                    .in_set(UiSystemSet::InGame),
            )
            .add_system(current_building_window.in_set(UiSystemSet::InGame))
            .add_systems(
                (reset_hovers_window, update_last_cursor_position).in_set(UiSystemSet::InGameFirst),
            );
    }
}

#[derive(Default, Deref, DerefMut, Resource, Eq, PartialEq)]
pub struct HoveringAboveWindow(pub bool);

#[derive(Default, Resource)]
pub struct LastCursorPosition(Vec2);

#[derive(Default, EnumIter)]
pub enum InfoWindow {
    #[default]
    BuildingSelection,
    Plots,
    Research,
    ShippingOptions,
}

impl InfoWindow {
    pub fn toggle(&self, windows_open: &mut WindowsOpen) {
        match self {
            InfoWindow::BuildingSelection => {
                windows_open.building_selection_window = !windows_open.building_selection_window;
            }
            InfoWindow::Plots => {
                windows_open.plots_window = !windows_open.plots_window;
            }
            InfoWindow::Research => {
                windows_open.research_window = !windows_open.research_window;
            }
            InfoWindow::ShippingOptions => {
                windows_open.shipping_window = !windows_open.shipping_window;
            }
        }
    }

    pub fn tool_tip(&self) -> String {
        match self {
            InfoWindow::BuildingSelection => "Building Selection".to_string(),
            InfoWindow::Plots => "Plots".to_string(),
            InfoWindow::Research => "Research".to_string(),
            InfoWindow::ShippingOptions => "Shipping Options".to_string(),
        }
    }
}

impl Display for InfoWindow {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            InfoWindow::BuildingSelection => write!(f, "\u{2692}"),
            InfoWindow::Plots => write!(f, "\u{1f4c8}"),
            InfoWindow::Research => write!(f, "\u{1f393}"),
            InfoWindow::ShippingOptions => write!(f, "\u{1f69a}"),
        }
    }
}

pub fn hovers_window(hovers_window: Res<HoveringAboveWindow>) -> bool {
    hovers_window.0
}

pub fn reset_hovers_window(mut hovers_window: ResMut<HoveringAboveWindow>) {
    hovers_window.0 = false;
}

fn update_last_cursor_position(
    mut last_cursor_position: ResMut<LastCursorPosition>,
    mut cursor_moved: EventReader<CursorMoved>,
) {
    if let Some(event) = cursor_moved.iter().last() {
        last_cursor_position.0 = event.position;
    }
}

fn set_buildings_disabled(mut windows_open: ResMut<WindowsOpen>) {
    windows_open.buildings_enabled = false;
}

fn set_buildings_enabled(mut windows_open: ResMut<WindowsOpen>) {
    windows_open.buildings_enabled = true;
}

fn close_all(mut windows_open: ResMut<WindowsOpen>) {
    *windows_open = WindowsOpen::default();
}

pub fn update_window_hovering(
    response: Option<InnerResponse<Option<()>>>,
    last_cursor_position: &LastCursorPosition,
    window: &Window,
    hovering_above_window: &mut bool,
) {
    if let Some(inner_response) = response {
        let rect = inner_response.response.rect;
        if rect.contains(Pos2::new(
            last_cursor_position.0.x,
            window.height() - last_cursor_position.0.y,
        )) {
            *hovering_above_window = true;
        }
    }
}
