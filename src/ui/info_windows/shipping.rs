pub use bevy::prelude::*;
use bevy_egui::egui::Context;
use bevy_egui::{egui, EguiContexts};
use strum::IntoEnumIterator;

use crate::core::shipping_options::ShippingOption;
use crate::ui::info_windows::{
    update_window_hovering, HoveringAboveWindow, LastCursorPosition, WindowsOpen,
};
use crate::ui::ui_plugin::nice_button;

#[derive(Resource, Default)]
pub struct ShippingWindow;

impl ShippingWindow {
    pub fn display(
        &self,
        ctx: &Context,
        windows_open: &mut WindowsOpen,
        window: &Window,
        last_cursor_position: &LastCursorPosition,
        hovering_above_window: &mut HoveringAboveWindow,
        shipping_option: &mut ShippingOption,
    ) {
        let response = egui::Window::new("Shipping")
            .open(&mut windows_open.shipping_window)
            .resizable(false)
            .show(ctx, |ui| {
                ui.vertical_centered_justified(|ui| {
                    for option in ShippingOption::iter() {
                        let enabled = *shipping_option != option;
                        if ui
                            .add_enabled(enabled, nice_button(option.to_string()))
                            .on_disabled_hover_text(option.tooltip())
                            .on_hover_text(option.tooltip())
                            .clicked()
                        {
                            *shipping_option = option;
                        }
                    }
                });
            });

        update_window_hovering(
            response,
            last_cursor_position,
            window,
            hovering_above_window,
        );
    }
}

pub fn shipping_window(
    window: Query<&Window>,
    last_cursor_position: Res<LastCursorPosition>,
    mut shipping_option: ResMut<ShippingOption>,
    mut hovering_above_window: ResMut<HoveringAboveWindow>,
    mut windows_open: ResMut<WindowsOpen>,
    mut contexts: EguiContexts,
    shipping_window: Res<ShippingWindow>,
) {
    shipping_window.display(
        contexts.ctx_mut(),
        &mut windows_open,
        window.single(),
        &last_cursor_position,
        &mut hovering_above_window,
        &mut shipping_option,
    );
}
