use bevy::prelude::*;
use bevy_egui::egui::{Color32, Context};
use bevy_egui::{egui, EguiContexts};
use strum::IntoEnumIterator;

use crate::core::research::{GridUpgrades, ResearchItem, ResearchTree};
use crate::core::resources::Resources;
use crate::core::terrain_grid::terrain::GameTerrain;
use crate::ui::info_windows::{
    update_window_hovering, HoveringAboveWindow, LastCursorPosition, WindowsOpen,
};
use crate::ui::ui_plugin::nice_button;

#[derive(Default, Resource)]
pub struct ResearchWindow;

impl ResearchWindow {
    pub fn display(
        &mut self,
        window: &Window,
        last_cursor_position: &LastCursorPosition,
        research_tree: &mut ResearchTree,
        resources: &mut Resources,
        grid_upgrades: &mut GridUpgrades,
        terrain: &mut GameTerrain,
        hovering_above_window: &mut HoveringAboveWindow,
        windows_open: &mut WindowsOpen,
        ctx: &Context,
    ) {
        let response = egui::Window::new("Research")
            .open(&mut windows_open.research_window)
            .resizable(false)
            .show(ctx, |ui| {
                let resources_mut = &mut *resources;
                for research in ResearchItem::iter() {
                    match research {
                        ResearchItem::NewRow(_) => {
                            let research_item =
                                ResearchItem::NewRow(grid_upgrades.rows.min(
                                    terrain.terrain_grid.rows() - grid_upgrades.start_rows - 1,
                                ));
                            let enabled = research_tree.can_research(research_item, *resources_mut);
                            let color = if research_tree.is_unlocked(research_item) {
                                Color32::DARK_GREEN
                            } else if enabled {
                                Color32::from_rgb(23, 178, 43)
                            } else {
                                Color32::RED
                            };

                            ui.vertical_centered_justified(|ui| {
                                if ui
                                    .add_enabled(
                                        enabled,
                                        nice_button(research_item.to_string()).fill(color),
                                    )
                                    .on_hover_text(research_item.tooltip(research_tree))
                                    .on_disabled_hover_text(research_item.tooltip(research_tree))
                                    .clicked()
                                {
                                    research_tree.try_unlock(research_item, resources_mut);
                                    grid_upgrades.rows += 1;
                                    terrain.enable_row();
                                }
                            });
                        }
                        ResearchItem::NewColumn(_) => {
                            let research_item = ResearchItem::NewColumn(grid_upgrades.columns.min(
                                terrain.terrain_grid.cols() - grid_upgrades.start_columns - 1,
                            ));
                            let enabled = research_tree.can_research(research_item, *resources_mut);
                            let color = if research_tree.is_unlocked(research_item) {
                                Color32::DARK_GREEN
                            } else if enabled {
                                Color32::from_rgb(23, 178, 43)
                            } else {
                                Color32::RED
                            };

                            ui.vertical_centered_justified(|ui| {
                                if ui
                                    .add_enabled(
                                        enabled,
                                        nice_button(research_item.to_string()).fill(color),
                                    )
                                    .on_hover_text(research_item.tooltip(research_tree))
                                    .on_disabled_hover_text(research_item.tooltip(research_tree))
                                    .clicked()
                                {
                                    research_tree.try_unlock(research_item, resources_mut);
                                    grid_upgrades.columns += 1;
                                    terrain.enable_column();
                                }
                            });
                        }
                        _ => {
                            let enabled = research_tree.can_research(research, *resources_mut);
                            let color = if research_tree.is_unlocked(research) {
                                Color32::DARK_GREEN
                            } else if enabled {
                                Color32::from_rgb(23, 178, 43)
                            } else {
                                Color32::RED
                            };
                            ui.vertical_centered_justified(|ui| {
                                if ui
                                    .add_enabled(
                                        enabled,
                                        nice_button(research.to_string()).fill(color),
                                    )
                                    .on_hover_text(research.tooltip(research_tree))
                                    .on_disabled_hover_text(research.tooltip(research_tree))
                                    .clicked()
                                {
                                    research_tree.try_unlock(research, resources_mut);
                                }
                            });
                        }
                    }
                }
            });

        update_window_hovering(
            response,
            last_cursor_position,
            window,
            hovering_above_window,
        );
    }
}

pub fn research_window(
    window: Query<&Window>,
    last_cursor_position: Res<LastCursorPosition>,
    mut research_tree: ResMut<ResearchTree>,
    mut resources: ResMut<Resources>,
    mut grid_upgrades: ResMut<GridUpgrades>,
    mut terrain: ResMut<GameTerrain>,
    mut hovering_above_window: ResMut<HoveringAboveWindow>,
    mut windows_open: ResMut<WindowsOpen>,
    mut contexts: EguiContexts,
    mut research_window: ResMut<ResearchWindow>,
) {
    research_window.display(
        window.single(),
        &last_cursor_position,
        &mut research_tree,
        &mut resources,
        &mut grid_upgrades,
        &mut terrain,
        &mut hovering_above_window,
        &mut windows_open,
        contexts.ctx_mut(),
    );
}
