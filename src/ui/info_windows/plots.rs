use std::fmt::{Display, Formatter};
use std::hash::Hash;
use std::ops::RangeInclusive;

use bevy::prelude::*;
use bevy_egui::egui::plot::{HLine, Line, Plot, PlotPoint, PlotPoints, PlotUi, VLine};
use bevy_egui::egui::{Color32, Context, Ui};
use bevy_egui::{egui, EguiContexts};
use quantities::currency::Currency;
use quantities::duration::{DAY, MONTH};
use quantities::energy::{Energy, GIGAWATT_HOUR, KILOWATT_HOUR, WATT_HOUR};
use quantities::mass::Mass;
use quantities::power::Power;
use quantities::{HasRefUnit, Quantity};
use strum::IntoEnumIterator;
use strum_macros::EnumIter;

use crate::core::buildings::Building;
use crate::core::months::FinalMonth;
use crate::core::resources::{Reputation, Resources, PERCENT};
use crate::core::statistics::Statistics;
use crate::core::time_scale::IngameTime;
use crate::core::upgrades::UpgradeStage;
use crate::ui::info_windows::{
    update_window_hovering, HoveringAboveWindow, LastCursorPosition, WindowsOpen,
};
use crate::ui::QuantityExt;

#[derive(Copy, Clone, Debug, Default, EnumIter, Eq, PartialEq)]
pub enum PlotPanel {
    #[default]
    CO2,
    Power,
    Energy,
    Money,
    Reputation,
}

impl PlotPanel {
    pub fn push_statistic(
        &self,
        ingame_time: &IngameTime,
        resources: &Resources,
        statistics: &mut Statistics,
    ) {
        let time = (ingame_time.0 * MONTH).amount();

        match self {
            PlotPanel::CO2 => statistics.co2_emissions.push(PlotPoint::new(
                time,
                resources.co2.equiv_amount(Mass::DEFAULT_UNIT),
            )),
            PlotPanel::Power => statistics.power_production.push(PlotPoint::new(
                time,
                resources.power.equiv_amount(Power::DEFAULT_UNIT),
            )),
            PlotPanel::Energy => statistics.energy_stored.push(PlotPoint::new(
                time,
                resources.energy.equiv_amount(Energy::DEFAULT_UNIT),
            )),
            PlotPanel::Money => statistics.money.push(PlotPoint::new(
                time,
                resources.money.equiv_amount(Currency::DEFAULT_UNIT),
            )),
            PlotPanel::Reputation => statistics.reputation.push(PlotPoint::new(
                time,
                resources.reputation.get_value().equiv_amount(PERCENT),
            )),
        }
    }
}

impl Display for PlotPanel {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            PlotPanel::CO2 => write!(f, "CO₂"),
            PlotPanel::Power => write!(f, "Power"),
            PlotPanel::Energy => write!(f, "Energy"),
            PlotPanel::Money => write!(f, "Money"),
            PlotPanel::Reputation => write!(f, "Reputation"),
        }
    }
}

#[derive(Default, Resource)]
pub struct PlotsWindow {
    pub open_panel: PlotPanel,
}

impl PlotsWindow {
    pub fn display(
        &mut self,
        window: &Window,
        last_cursor_position: &LastCursorPosition,
        statistics: &Statistics,
        ingame_time: &IngameTime,
        final_month: &FinalMonth,
        buildings: Query<(&Building, Option<&UpgradeStage>)>,
        hovering_above_window: &mut HoveringAboveWindow,
        windows_open: &mut WindowsOpen,
        ctx: &Context,
    ) {
        let response = egui::Window::new("Plots")
            .open(&mut windows_open.plots_window)
            .resizable(false)
            .show(ctx, |ui| {
                ui.horizontal(|ui| {
                    for panel in PlotPanel::iter() {
                        ui.selectable_value(&mut self.open_panel, panel, panel.to_string());
                    }
                });

                ui.separator();

                let max_energy = buildings
                    .iter()
                    .map(|(b, u)| match b {
                        Building::Batteries { .. } => b.max_energy(u),
                        _ => 0.0 * WATT_HOUR,
                    })
                    .fold(0.0 * WATT_HOUR, |acc, e| acc + e);

                match self.open_panel {
                    PlotPanel::CO2 => self.plot_co2(statistics, ingame_time, ui),
                    PlotPanel::Power => self.plot_power(statistics, ingame_time, ui),
                    PlotPanel::Energy => {
                        self.plot_energy(statistics, ingame_time, final_month, max_energy, ui)
                    }
                    PlotPanel::Money => self.plot_money(statistics, ingame_time, final_month, ui),
                    PlotPanel::Reputation => {
                        self.plot_reputation(statistics, ingame_time, final_month, ui)
                    }
                }
            });

        update_window_hovering(
            response,
            last_cursor_position,
            window,
            hovering_above_window,
        );
    }

    fn month(time: f64) -> f64 {
        time.floor() + 1.0
    }

    fn day(time: f64) -> f64 {
        ((time * MONTH).equiv_amount(DAY) % (1.0 * MONTH).equiv_amount(DAY)).floor() + 1.0
    }

    fn approx_zero(x: f64) -> bool {
        x.abs() < 1e-6
    }

    fn approx_integer(x: f64) -> bool {
        x.fract().abs() < 1e-6
    }

    pub fn plot_ingame_time(ingame_time: &IngameTime, plot_ui: &mut PlotUi) {
        plot_ui.vline(
            VLine::new(ingame_time.0)
                .name("Current Time")
                .color(Color32::from_white_alpha(50)),
        )
    }

    pub fn plot_target(points: &[PlotPoint], name: &str, plot_ui: &mut PlotUi) {
        plot_ui.line(
            Line::new(PlotPoints::Owned(points.to_owned()))
                .name(name)
                .color(Color32::LIGHT_GREEN),
        )
    }

    pub fn plot_production(points: &[PlotPoint], name: &str, plot_ui: &mut PlotUi) {
        plot_ui.line(
            Line::new(PlotPoints::Owned(points.to_owned()))
                .name(name)
                .color(Color32::LIGHT_BLUE)
                .fill(0.0),
        )
    }

    fn plot<Q>(&self, id: impl Hash + Sized) -> Plot
    where
        Q: QuantityExt,
    {
        let x_axis_formatter = |x, _: &RangeInclusive<f64>| {
            if Self::approx_integer(x) {
                format!("Month {}", Self::month(x))
            } else {
                format!("Day {}", Self::day(x))
            }
        };

        let y_axis_formatter = |y: f64, _: &RangeInclusive<f64>| {
            if !Self::approx_zero(y) && Self::approx_integer(y) {
                Q::new(y, Q::DEFAULT_UNIT).to_formatted_string()
            } else {
                String::new()
            }
        };

        let label_formatter = |s: &str, point: &PlotPoint| {
            let time = point.x;

            let month = Self::month(time);
            let day = Self::day(time);

            let value = Q::new(point.y, Q::DEFAULT_UNIT).to_formatted_string();

            format!("{s}\nMonth {month}, Day {day}\n{value}")
        };

        Plot::new(id)
            .height(256.0)
            .x_axis_formatter(x_axis_formatter)
            .y_axis_formatter(y_axis_formatter)
            .label_formatter(label_formatter)
            .include_x(0.0)
            .include_y(0.0)
            .auto_bounds_y()
    }

    pub fn plot_co2(&self, statistics: &Statistics, ingame_time: &IngameTime, ui: &mut Ui) {
        let plot = self.plot::<Mass>("co2-plot");

        plot.show(ui, |plot_ui| {
            Self::plot_target(&statistics.co2_targets, "Target Emissions", plot_ui);
            Self::plot_production(&statistics.co2_emissions, "Emissions", plot_ui);
            Self::plot_ingame_time(ingame_time, plot_ui);
        });
    }

    pub fn plot_power(&self, statistics: &Statistics, ingame_time: &IngameTime, ui: &mut Ui) {
        let plot = self.plot::<Power>("power-plot");

        plot.show(ui, |plot_ui| {
            Self::plot_target(&statistics.power_targets, "Demand", plot_ui);
            Self::plot_production(&statistics.power_production, "Production", plot_ui);
            Self::plot_ingame_time(ingame_time, plot_ui);
        });
    }

    pub fn plot_energy(
        &self,
        statistics: &Statistics,
        ingame_time: &IngameTime,
        final_month: &FinalMonth,
        max_energy: Energy,
        ui: &mut Ui,
    ) {
        let plot = self
            .plot::<Energy>("energy-plot")
            .include_x(final_month.0 as f64)
            .include_y((12.0 * GIGAWATT_HOUR).equiv_amount(Energy::DEFAULT_UNIT));

        plot.show(ui, |plot_ui| {
            if max_energy > 1.0 * KILOWATT_HOUR {
                plot_ui.hline(
                    HLine::new(max_energy.equiv_amount(Energy::DEFAULT_UNIT))
                        .name("Maximum Storable Energy")
                        .color(Color32::LIGHT_GREEN),
                );
            }
            Self::plot_production(&statistics.energy_stored, "Energy", plot_ui);
            Self::plot_ingame_time(ingame_time, plot_ui);
        });
    }

    pub fn plot_money(
        &self,
        statistics: &Statistics,
        ingame_time: &IngameTime,
        final_month: &FinalMonth,
        ui: &mut Ui,
    ) {
        let plot = self
            .plot::<Currency>("money-plot")
            .include_x(final_month.0 as f64)
            .include_y(
                Resources::default()
                    .money
                    .equiv_amount(Currency::DEFAULT_UNIT)
                    * 2.0,
            );

        plot.show(ui, |plot_ui| {
            Self::plot_production(&statistics.money, "Money", plot_ui);
            Self::plot_ingame_time(ingame_time, plot_ui);
        });
    }

    pub fn plot_reputation(
        &self,
        statistics: &Statistics,
        ingame_time: &IngameTime,
        final_month: &FinalMonth,
        ui: &mut Ui,
    ) {
        let plot = self
            .plot::<Reputation>("reputation-plot")
            .include_x(final_month.0 as f64)
            .include_y(-100.0)
            .include_y(100.0);

        plot.show(ui, |plot_ui| {
            Self::plot_production(&statistics.reputation, "Reputation", plot_ui);
            Self::plot_ingame_time(ingame_time, plot_ui);
        });
    }
}

pub fn plots_window(
    window: Query<&Window>,
    last_cursor_position: Res<LastCursorPosition>,
    statistics: Res<Statistics>,
    ingame_time: Res<IngameTime>,
    final_month: Res<FinalMonth>,
    buildings: Query<(&Building, Option<&UpgradeStage>)>,
    mut hovering_above_window: ResMut<HoveringAboveWindow>,
    mut windows_open: ResMut<WindowsOpen>,
    mut contexts: EguiContexts,
    mut plots_window: ResMut<PlotsWindow>,
) {
    plots_window.display(
        window.single(),
        &last_cursor_position,
        &statistics,
        &ingame_time,
        &final_month,
        buildings,
        &mut hovering_above_window,
        &mut windows_open,
        contexts.ctx_mut(),
    );
}
