use bevy::prelude::*;
use bevy_egui::egui::Context;
use bevy_egui::{egui, EguiContexts};

use crate::core::buildings::Building;
use crate::core::resources::Resources;
use crate::core::shipping_options::ShippingOption;
use crate::core::terrain_grid::terrain::GameTerrain;
use crate::core::time_scale::IngameTime;
use crate::core::upgrades::{Upgrade, UpgradeEvent, UpgradeStage};
use crate::ui::info_windows::{update_window_hovering, HoveringAboveWindow, LastCursorPosition};
use crate::ui::ui_plugin::nice_button;
use crate::ui::QuantityExt;

#[derive(Default, Copy, Clone, Resource)]
pub struct CurrentBuildingWindow {
    pub entity: Option<Entity>,
    pub position: Vec2,
    pub dimensions: Rect,
}

impl CurrentBuildingWindow {
    fn display(
        &mut self,
        mut commands: Commands,
        window: &Window,
        last_cursor_position: &LastCursorPosition,
        hovering_above_window: &mut HoveringAboveWindow,
        buildings: Query<(&Building, &Transform, Option<&UpgradeStage>)>,
        game_terrain: &mut GameTerrain,
        ctx: &Context,
        light: &DirectionalLight,
        ingame_time: IngameTime,
        resources: &mut Resources,
        shipping_option: &ShippingOption,
        camera: (&Camera, &GlobalTransform),
        mut event_writer: EventWriter<UpgradeEvent>,
    ) {
        if self.entity.is_none() {
            return;
        }

        let mut open = true;

        let entity = self.entity.unwrap();
        let (building, transform, upgrade) = buildings.get(entity).unwrap();
        let (x, z) = game_terrain.world_to_grid(transform.translation);
        let grid_item = game_terrain.terrain_grid[z][x];

        let (camera, global_transform) = camera;

        let screen_space_coordinate = camera.world_to_viewport(
            global_transform,
            transform.translation + game_terrain.tile_size * Vec3::Y,
        );

        if let Some(screen_space_coordinate) = screen_space_coordinate {
            self.position = screen_space_coordinate
                + Vec2::new(-self.dimensions.half_size().x, self.dimensions.size().y);
            self.position = self.position.clamp(
                Vec2::new(0.0, self.dimensions.height()),
                Vec2::new(window.width() - self.dimensions.width(), window.height()),
            )
        }

        let response = egui::Window::new("Current Building")
            .title_bar(false)
            .open(&mut open)
            .resizable(false)
            .movable(false)
            .current_pos([self.position.x, window.height() - self.position.y])
            .show(ctx, |ui| {
                ui.label(format!("Level {} {}", upgrade.upgrade_number(), building));
                egui::Grid::new("current_building_grid").show(ui, |ui| {
                    ui.label("CO₂");
                    ui.label(building.emissions(upgrade).to_formatted_string());
                    ui.end_row();

                    ui.label("Power");
                    ui.label(
                        building
                            .power(
                                transform.translation,
                                grid_item.terrain_tile,
                                *ingame_time as f32,
                                light.illuminance as f64,
                                upgrade,
                            )
                            .to_formatted_string(),
                    );
                    ui.end_row();

                    if let Building::Batteries { energy } = building {
                        ui.label("Energy");
                        ui.label(format!(
                            "{} / {}",
                            energy.to_formatted_string(),
                            building.max_energy(upgrade).to_formatted_string()
                        ));
                        ui.end_row();
                    }
                });

                let spacing = 5.0;
                egui::Grid::new("current_building_button_grid")
                    .spacing([spacing, spacing])
                    .min_col_width(ui.available_width() / 2.0 - spacing)
                    .max_col_width(ui.available_width() / 2.0 - spacing)
                    .show(ui, |ui| {
                        let enough_money =
                            resources.money >= building.destroy_price(shipping_option);
                        let tooltip = format!(
                            "Costs: {}, Emissions: {}",
                            building
                                .destroy_price(shipping_option)
                                .to_formatted_string(),
                            building
                                .destroy_emissions(shipping_option)
                                .to_formatted_string()
                        );

                        ui.vertical_centered_justified(|ui| {
                            if ui
                                .add_enabled(enough_money, nice_button("Destroy"))
                                .on_hover_text(tooltip.clone())
                                .on_disabled_hover_text(tooltip)
                                .clicked()
                            {
                                let entity = game_terrain.remove(transform.translation).unwrap();
                                self.entity = None;
                                commands.entity(entity).despawn_recursive();

                                resources.destroy(building, shipping_option);
                            }
                        });

                        ui.vertical_centered_justified(|ui| {
                            let enough_money = resources.money
                                >= upgrade.costs() * building.price(&ShippingOption::default())
                                && upgrade != Some(&UpgradeStage::Third);

                            let tooltip = building.upgrade_tooltip(upgrade);

                            if ui
                                .add_enabled(enough_money, nice_button("Upgrade"))
                                .on_hover_text(tooltip.clone())
                                .on_disabled_hover_text(tooltip)
                                .clicked()
                            {
                                event_writer.send(UpgradeEvent(entity))
                            }
                        });
                    });
            });

        if let Some(r) = &response {
            self.dimensions = emath_rect_to_bevy(&r.response.rect);
        }

        if !open {
            self.entity = None
        } else {
            update_window_hovering(
                response,
                last_cursor_position,
                window,
                hovering_above_window,
            );
        }
    }
}

pub fn current_building_window(
    mut current_building_window: ResMut<CurrentBuildingWindow>,
    commands: Commands,
    window: Query<&Window>,
    last_cursor_position: Res<LastCursorPosition>,
    mut hovering_above_window: ResMut<HoveringAboveWindow>,
    buildings: Query<(&Building, &Transform, Option<&UpgradeStage>)>,
    mut game_terrain: ResMut<GameTerrain>,
    mut contexts: EguiContexts,
    light: Query<&DirectionalLight>,
    ingame_time: Res<IngameTime>,
    mut resources: ResMut<Resources>,
    shipping_option: Res<ShippingOption>,
    camera_query: Query<(&Camera, &GlobalTransform)>,
    event_writer: EventWriter<UpgradeEvent>,
) {
    current_building_window.display(
        commands,
        window.single(),
        &last_cursor_position,
        &mut hovering_above_window,
        buildings,
        &mut game_terrain,
        contexts.ctx_mut(),
        light.single(),
        *ingame_time,
        &mut resources,
        &shipping_option,
        camera_query.single(),
        event_writer,
    )
}

fn emath_rect_to_bevy(rect: &egui::emath::Rect) -> Rect {
    Rect::new(rect.min.x, rect.min.y, rect.max.x, rect.max.y)
}
