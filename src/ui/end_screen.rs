use std::fmt::{Display, Formatter};

use bevy::app::AppExit;
use bevy::prelude::*;
use bevy_egui::egui::{Frame, Pos2, RichText, Ui};
use bevy_egui::{egui, EguiContexts};
use quantities::energy::KILOWATT_HOUR;
use strum::IntoEnumIterator;

use crate::core::months::FinalMonth;
use crate::core::states::GameState;
use crate::core::statistics::Statistics;
use crate::core::time_scale::IngameTime;
use crate::ui::info_windows::current_building::CurrentBuildingWindow;
use crate::ui::info_windows::plots::{PlotPanel, PlotsWindow};
use crate::ui::main_menu::set_main_menu_style;
use crate::ui::ui_plugin::nice_button;

pub struct EndScreenPlugin;

impl Plugin for EndScreenPlugin {
    fn build(&self, app: &mut App) {
        app.add_system(loss_screen.in_set(OnUpdate(GameState::GameLost)))
            .add_system(win_screen.in_set(OnUpdate(GameState::GameWon)))
            .add_system(set_main_menu_style.in_schedule(OnEnter(GameState::GameLost)))
            .add_system(set_main_menu_style.in_schedule(OnEnter(GameState::GameWon)));
    }
}

#[derive(Resource)]
pub enum LossReason {
    Power,
    NegativeRep,
}

impl Display for LossReason {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            LossReason::Power => write!(f, "You didn't produce enough power to satisfy the needs of the city."),
            LossReason::NegativeRep => write!(f, "You had a negative amount of money or reputation two months in a row.\nThe citizens decided to lynch you to enforce having someone different to be responsible for satisfying their needs,")
        }
    }
}

fn loss_screen(
    mut contexts: EguiContexts,
    mut exit: EventWriter<AppExit>,
    mut next_state: ResMut<NextState<GameState>>,
    mut current_building_window: ResMut<CurrentBuildingWindow>,
    loss_reason: Res<LossReason>,
    window: Query<&Window>,
    mut plots_window: ResMut<PlotsWindow>,
    statistics: Res<Statistics>,
    ingame_time: Res<IngameTime>,
    final_month: Res<FinalMonth>,
) {
    let ctx = contexts.ctx_mut();
    let window_size = [window.single().width(), window.single().height()];
    egui::Window::new("end_screen")
        .frame(Frame::none())
        .fixed_rect(egui::Rect::from_center_size(
            Pos2::from([window_size[0] / 2.0, window_size[1] / 2.0]),
            [window_size[0] / 3.0, window_size[1] / 1.2].into(),
        ))
        .title_bar(false)
        .show(ctx, |ui| {
            ui.vertical_centered(|ui| {
                ui.heading("You Lost! \u{26B0}");
                ui.label(loss_reason.to_string());
                ui.add_space(20.0);

                if ui.add(nice_button("Quit to Main Menu")).clicked() {
                    current_building_window.entity = None;
                    next_state.set(GameState::Menu)
                }

                if ui.add(nice_button("Quit")).clicked() {
                    exit.send(AppExit);
                };

                end_plot(
                    ui,
                    &mut plots_window,
                    &statistics,
                    &ingame_time,
                    &final_month,
                );
            });
        });
}

fn win_screen(
    mut contexts: EguiContexts,
    mut exit: EventWriter<AppExit>,
    mut next_state: ResMut<NextState<GameState>>,
    mut current_building_window: ResMut<CurrentBuildingWindow>,
    window: Query<&Window>,
    mut plots_window: ResMut<PlotsWindow>,
    statistics: Res<Statistics>,
    ingame_time: Res<IngameTime>,
    final_month: Res<FinalMonth>,
) {
    let ctx = contexts.ctx_mut();
    let window_size = [window.single().width(), window.single().height()];
    egui::Window::new("end_screen")
        .frame(Frame::none())
        .fixed_rect(egui::Rect::from_center_size(
            Pos2::from([window_size[0] / 2.0, window_size[1] / 2.0]),
            [window_size[0] / 3.0, window_size[1] / 1.2].into(),
        ))
        .title_bar(false)
        .show(ctx, |ui| {
            ui.vertical_centered(|ui| {
                ui.add_space(200.0);

                ui.heading("\u{1f389} You Won! \u{1f389}");

                ui.add_space(20.0);

                if ui.add(nice_button("Quit to Main Menu")).clicked() {
                    current_building_window.entity = None;
                    next_state.set(GameState::Menu)
                }

                if ui.add(nice_button("Quit")).clicked() {
                    exit.send(AppExit);
                };

                end_plot(
                    ui,
                    &mut plots_window,
                    &statistics,
                    &ingame_time,
                    &final_month,
                );
            });
        });
}

fn end_plot(
    ui: &mut Ui,
    plots_window: &mut PlotsWindow,
    statistics: &Statistics,
    ingame_time: &IngameTime,
    final_month: &FinalMonth,
) {
    ui.horizontal(|ui| {
        for panel in PlotPanel::iter() {
            ui.selectable_value(
                &mut plots_window.open_panel,
                panel,
                RichText::new(panel.to_string()).size(20.0),
            );
        }
    });

    ui.separator();

    match plots_window.open_panel {
        PlotPanel::CO2 => plots_window.plot_co2(statistics, ingame_time, ui),
        PlotPanel::Power => plots_window.plot_power(statistics, ingame_time, ui),
        PlotPanel::Energy => plots_window.plot_energy(
            statistics,
            ingame_time,
            final_month,
            0.0 * KILOWATT_HOUR,
            ui,
        ),
        PlotPanel::Money => plots_window.plot_money(statistics, ingame_time, final_month, ui),
        PlotPanel::Reputation => {
            plots_window.plot_reputation(statistics, ingame_time, final_month, ui)
        }
    }
}
