use bevy::app::AppExit;
use bevy::prelude::*;
use bevy_egui::egui::FontFamily::Proportional;
use bevy_egui::egui::TextStyle::*;
use bevy_egui::egui::{emath, FontFamily, FontId, Margin, Pos2, Rounding};
use bevy_egui::{egui, EguiContexts};

use crate::core::states::GameState;
use crate::ui::info_windows::current_building::CurrentBuildingWindow;
use crate::ui::ui_plugin::nice_button;

pub struct PauseMenuPlugin;

impl Plugin for PauseMenuPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<PauseMenuWindow>()
            .add_system(set_pause.in_set(OnUpdate(GameState::InGame)))
            .add_systems((set_play, pause_menu).in_set(OnUpdate(GameState::Paused)))
            .add_system(set_pause_menu_style.in_schedule(OnEnter(GameState::PreGame)));
    }
}

#[derive(Resource)]
pub struct PauseMenuWindow {
    position: emath::Rect,
}

impl FromWorld for PauseMenuWindow {
    fn from_world(world: &mut World) -> Self {
        let window = world.query::<&Window>().single(world);
        // Size at my screen. Default value to make flickering on first pause menu frame less visible. (maybe invisible).
        PauseMenuWindow {
            position: emath::Rect::from_center_size(
                Pos2::new(window.width() / 2.0, window.height() / 2.0),
                emath::Vec2::new(352.0, 79.0),
            ),
        }
    }
}

fn set_pause(mut paused: ResMut<NextState<GameState>>, keys: Res<Input<KeyCode>>) {
    if keys.just_pressed(KeyCode::Escape) {
        paused.set(GameState::Paused)
    }
}

fn set_play(mut paused: ResMut<NextState<GameState>>, keys: Res<Input<KeyCode>>) {
    if keys.just_pressed(KeyCode::Escape) {
        paused.set(GameState::InGame)
    }
}

fn pause_menu(
    mut contexts: EguiContexts,
    mut app_exit: EventWriter<AppExit>,
    mut pause_menu_window: ResMut<PauseMenuWindow>,
    mut next_state: ResMut<NextState<GameState>>,
    mut current_building_window: ResMut<CurrentBuildingWindow>,
    bevy_window: Query<&Window>,
) {
    let center_of_screen = Pos2::new(
        bevy_window.single().width() / 2.0,
        bevy_window.single().height() / 2.0,
    );
    let half_window_dimension = egui::Vec2::new(
        pause_menu_window.position.width() / 2.0,
        pause_menu_window.position.height() / 2.0,
    );
    let response = egui::Window::new("Game Paused")
        .collapsible(false)
        .resizable(false)
        .current_pos(center_of_screen - half_window_dimension)
        .show(contexts.ctx_mut(), |ui| {
            ui.vertical_centered(|ui| {
                if ui.add(nice_button("Quit to Main Menu")).clicked() {
                    current_building_window.entity = None;
                    next_state.set(GameState::Menu);
                }
                if ui.add(nice_button("Quit Game")).clicked() {
                    app_exit.send(AppExit);
                }
            })
        });
    if let Some(inner_response) = response {
        pause_menu_window.position = inner_response.response.rect;
    }
}

fn set_pause_menu_style(mut contexts: EguiContexts) {
    let ctx = contexts.ctx_mut();
    let mut style = (*ctx.style()).clone();
    style.text_styles = [
        (Heading, FontId::new(30.0, Proportional)),
        (Body, FontId::new(15.0, Proportional)),
        (Monospace, FontId::new(15.0, FontFamily::Monospace)),
        (egui::TextStyle::Button, FontId::new(20.0, Proportional)),
        (Small, FontId::new(10.0, Proportional)),
    ]
    .into();
    style.spacing.item_spacing = egui::Vec2::new(4.0, 4.0);
    style.visuals.menu_rounding = Rounding::same(3.0);
    style.visuals.window_rounding = Rounding::same(3.0);
    style.spacing.window_margin = Margin::same(5.0);
    ctx.set_style(style);
}
