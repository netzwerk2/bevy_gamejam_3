use bevy::prelude::*;
use bevy_egui::egui::epaint::Shadow;
use bevy_egui::egui::FontFamily::Proportional;
use bevy_egui::egui::TextStyle::{Body, Button, Heading, Monospace, Small};
use bevy_egui::egui::{Color32, FontFamily, FontId, Rounding, Stroke, TextureId, WidgetText};
use bevy_egui::{egui, EguiContexts};

use crate::core::months::MonthPlugin;
use crate::core::states::GameState;
use crate::core::statistics::StatisticsPlugin;
use crate::ui::bottom_panel::BottomPanelPlugin;
use crate::ui::end_screen::EndScreenPlugin;
use crate::ui::info_windows::InfoWindowPlugin;
use crate::ui::main_menu::MainMenuPlugin;
use crate::ui::pause_menu::PauseMenuPlugin;
use crate::ui::top_panel::TopPanelPlugin;
use crate::ui::tutorial::TutorialPlugin;

pub struct UIPlugin;

impl Plugin for UIPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugin(MonthPlugin)
            .add_plugin(StatisticsPlugin)
            .add_plugin(MainMenuPlugin)
            .add_plugin(PauseMenuPlugin)
            .add_plugin(BottomPanelPlugin)
            .add_plugin(TopPanelPlugin)
            .add_plugin(InfoWindowPlugin)
            .add_plugin(EndScreenPlugin)
            .add_plugin(TutorialPlugin)
            .add_startup_system(set_style)
            .configure_set(UiSystemSet::InGame.in_set(OnUpdate(GameState::InGame)))
            .configure_set(
                UiSystemSet::InGameFirst
                    .in_set(OnUpdate(GameState::InGame))
                    .before(UiSystemSet::InGame),
            );
    }
}

#[derive(SystemSet, Debug, Hash, PartialEq, Eq, Clone)]
pub enum UiSystemSet {
    InGameFirst,
    InGame,
}

fn set_style(mut contexts: EguiContexts) {
    let ctx = contexts.ctx_mut();
    let mut style = (*ctx.style()).clone();

    style.text_styles = [
        (Heading, FontId::new(40.0, Proportional)),
        (Body, FontId::new(20.0, Proportional)),
        (Monospace, FontId::new(20.0, FontFamily::Monospace)),
        (Button, FontId::new(20.0, Proportional)),
        (Small, FontId::new(10.0, Proportional)),
    ]
    .into();
    style.visuals.menu_rounding = Rounding::none();
    style.visuals.window_rounding = Rounding::same(2.0);
    style.visuals.window_stroke = Stroke::NONE;
    style.visuals.window_shadow = Shadow::NONE;
    style.visuals.override_text_color = Some(Color32::from_rgb(255, 255, 255));
    style.visuals.panel_fill = Color32::from_gray(0);
    style.visuals.window_fill = Color32::from_gray(0);
    ctx.set_style(style);
}

pub fn nice_button(text: impl Into<WidgetText>) -> egui::Button {
    egui::Button::new(text)
        .fill(Color32::from_gray(30))
        .rounding(Rounding::none())
}

pub fn image_button(image: TextureId, size: Vec2) -> egui::ImageButton {
    egui::ImageButton::new(image, [size.x, size.y])
}
