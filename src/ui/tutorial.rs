pub use bevy::prelude::*;
use bevy_egui::egui::style::Spacing;
use bevy_egui::egui::FontFamily::Proportional;
use bevy_egui::egui::TextStyle::{Body, Heading, Monospace, Small};
use bevy_egui::egui::{FontFamily, FontId, ScrollArea};
use bevy_egui::{egui, EguiContexts};
use egui_commonmark::CommonMarkViewer;

use crate::assets::DocsAssets;
use crate::core::states::GameState;
use crate::ui::ui_plugin::nice_button;

pub struct TutorialPlugin;

impl Plugin for TutorialPlugin {
    fn build(&self, app: &mut App) {
        app.add_system(set_tutorial_style.in_schedule(OnEnter(GameState::Tutorial)))
            .add_system(tutorial_viewer.in_set(OnUpdate(GameState::Tutorial)));
    }
}

fn tutorial_viewer(
    mut contexts: EguiContexts,
    mut next_state: ResMut<NextState<GameState>>,
    mut docs_assets: ResMut<DocsAssets>,
) {
    let ctx = contexts.ctx_mut();

    egui::CentralPanel::default().show(ctx, |ui| {
        let scroll_area_height = ui.available_height() - 42.0;

        egui::Grid::new("tutorial_grid")
            .num_columns(3)
            .min_col_width(ui.available_width() / 3.0)
            .max_col_width(ui.available_width() / 3.0)
            .show(ui, |ui| {
                ui.label("");

                ui.vertical_centered_justified(|ui| {
                    egui::Grid::new("tutorial_button_grid")
                        .num_columns(1)
                        .min_col_width(ui.available_width() - 10.0)
                        .spacing([5.0, 5.0])
                        .show(ui, |ui| {
                            ui.vertical_centered_justified(|ui| {
                                if ui.add(nice_button("Main Menu")).clicked() {
                                    next_state.set(GameState::Menu)
                                }
                            });
                        });

                    let tutorial_text = docs_assets.tutorial_text.clone();

                    ScrollArea::both()
                        .max_height(scroll_area_height)
                        .min_scrolled_width(ui.available_width())
                        .show(ui, |ui| {
                            CommonMarkViewer::new("tutorial-viewer").show(
                                ui,
                                &mut docs_assets.common_mark_cache,
                                &tutorial_text,
                            );
                        });

                    ui.label("");
                });
            });
    });
}

fn set_tutorial_style(mut contexts: EguiContexts) {
    let ctx = contexts.ctx_mut();
    let mut style = (*ctx.style()).clone();
    style.text_styles = [
        (Heading, FontId::new(25.0, Proportional)),
        (Body, FontId::new(14.0, Proportional)),
        (Monospace, FontId::new(15.0, FontFamily::Monospace)),
        (egui::TextStyle::Button, FontId::new(13.0, Proportional)),
        (Small, FontId::new(10.0, Proportional)),
    ]
    .into();
    style.spacing = Spacing {
        item_spacing: egui::Vec2::new(15.0, 15.0),
        ..default()
    };
    ctx.set_style(style);
}
