use bevy::prelude::*;
use bevy_egui::egui::TopBottomPanel;
use bevy_egui::EguiContexts;
use quantities::duration::MONTH;

use crate::core::months::ProductionTargets;
use crate::core::resources::Resources;
use crate::core::states::GameState;
use crate::core::time_scale::IngameTime;
use crate::ui::QuantityExt;

pub struct TopPanelPlugin;

impl Plugin for TopPanelPlugin {
    fn build(&self, app: &mut App) {
        app.add_system(top_panel.in_set(OnUpdate(GameState::InGame)))
            .add_system(top_panel.in_set(OnUpdate(GameState::Paused)));
    }
}

fn top_panel(
    mut contexts: EguiContexts,
    production_targets: Res<ProductionTargets>,
    resources: Res<Resources>,
    ingame_time: Res<IngameTime>,
) {
    let ctx = contexts.ctx_mut();
    TopBottomPanel::top("resources-panel")
        .resizable(false)
        .exact_height(30.0)
        .show(ctx, |ui| {
            ui.add_space(5.0);
            ui.horizontal(|ui| {
                let time = ingame_time.0 * MONTH;

                ui.label(format!(
                    "CO₂ Target: {}",
                    production_targets.co2_target(time).to_formatted_string()
                ));
                ui.add_space(5.0);
                ui.label(format!(
                    "Power Target: {}",
                    production_targets.power_target(time).to_formatted_string()
                ));

                ui.separator();

                ui.label(format!("Money: {}", resources.money.to_formatted_string()));
                ui.add_space(5.0);
                ui.label(format!("CO₂: {}", resources.co2.to_formatted_string()));
                ui.add_space(5.0);
                ui.label(format!(
                    "Reputation: {}",
                    resources.reputation.get_value().to_formatted_string()
                ));
                ui.add_space(5.0);
                ui.label(format!("Power: {}", resources.power.to_formatted_string()));
                ui.add_space(5.0);
                ui.label(format!(
                    "Energy: {}",
                    resources.energy.to_formatted_string()
                ));
            });
        });
}
