use bevy::prelude::*;
use bevy_egui::egui::FontFamily::Proportional;
use bevy_egui::egui::TextStyle::{Body, Button, Heading, Monospace, Small};
use bevy_egui::egui::{CentralPanel, FontFamily, FontId, Frame, Margin, Pos2, Rounding};
use bevy_egui::{egui, EguiContexts};

use crate::assets::Images;
use crate::core::states::GameState;
use crate::ui::ui_plugin::nice_button;

pub struct MainMenuPlugin;

impl Plugin for MainMenuPlugin {
    fn build(&self, app: &mut App) {
        app.add_system(main_menu.in_set(OnUpdate(GameState::Menu)))
            .add_system(set_main_menu_style.in_schedule(OnEnter(GameState::Menu)));
    }
}

fn main_menu(
    mut contexts: EguiContexts,
    mut next_state: ResMut<NextState<GameState>>,
    images: Res<Images>,
    window: Query<&Window>,
) {
    let ctx = contexts.ctx_mut();
    let window_size = [window.single().width(), window.single().height()];
    CentralPanel::default()
        .frame(Frame::none())
        .show(ctx, |ui| ui.image(images.main_menu_id, window_size));
    egui::Window::new("main_menu")
        .frame(Frame::none())
        .fixed_rect(egui::Rect::from_center_size(
            Pos2::from([window_size[0] / 2.0, window_size[1] / 2.0]),
            [window_size[0] / 3.0, window_size[1] / 2.0].into(),
        ))
        .title_bar(false)
        .show(ctx, |ui| {
            ui.vertical_centered_justified(|ui| {
                ui.heading("Main Menu");

                ui.add_space(20.0);

                if ui.add(nice_button("Start")).clicked() {
                    next_state.set(GameState::PreGame)
                };
                if ui.add(nice_button("Tutorial")).clicked() {
                    next_state.set(GameState::Tutorial)
                };
            });

            ui.label("");
        });
}

pub fn set_main_menu_style(mut contexts: EguiContexts) {
    let ctx = contexts.ctx_mut();
    let mut style = (*ctx.style()).clone();
    // Redefine text_styles
    style.text_styles = [
        (Heading, FontId::new(50.0, Proportional)),
        (Body, FontId::new(20.0, Proportional)),
        (Monospace, FontId::new(20.0, FontFamily::Monospace)),
        (Button, FontId::new(35.0, FontFamily::Monospace)),
        (Small, FontId::new(10.0, Proportional)),
    ]
    .into();
    style.visuals.menu_rounding = Rounding::none();
    style.visuals.window_rounding = Rounding::none();
    style.spacing.window_margin = Margin::from(0.0);
    // Mutate global style with above changes
    ctx.set_style(style);
}
