use bevy::prelude::*;

use bevy_gamejam_3::GamePlugin;

fn main() {
    #[cfg(target_arch = "wasm32")]
    console_error_panic_hook::set_once();

    App::new().add_plugin(GamePlugin).run();
}
