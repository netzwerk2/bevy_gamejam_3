use bevy::prelude::*;
use bevy_asset_loader::prelude::*;
use bevy_egui::egui::TextureId;
use bevy_egui::{EguiPlugin, EguiUserTextures};
use egui_commonmark::CommonMarkCache;

use crate::core::buildings::Building;
use crate::core::states::{GameState, StatePlugin};
use crate::core::terrain_grid::terrain::{TerrainTile, TerrainType};

pub struct AssetLoadingPlugin;

impl Plugin for AssetLoadingPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugin(StatePlugin)
            .add_plugin(EguiPlugin)
            .add_loading_state(
                LoadingState::new(GameState::Loading).continue_to_state(GameState::Menu),
            )
            .add_collection_to_loading_state::<_, BuildingAssets>(GameState::Loading)
            .init_resource::<TerrainAssets>()
            .init_resource::<BuildingThumbnails>()
            .init_resource::<Images>()
            .init_resource::<DocsAssets>();
    }
}

#[derive(AssetCollection, Resource)]
pub struct BuildingAssets {
    #[asset(path = "models/battery.gltf#Scene0")]
    pub batteries: Handle<Scene>,
    #[asset(path = "models/coal_plant.gltf#Scene0")]
    pub coal_plant: Handle<Scene>,
    #[asset(path = "models/gas_plant.gltf#Scene0")]
    pub gas_plant: Handle<Scene>,
    #[asset(path = "models/solar_array.gltf#Scene0")]
    pub solar_array: Handle<Scene>,
    #[asset(path = "models/wind_turbine.gltf#Scene0")]
    pub wind_turbines: Handle<Scene>,
    #[asset(path = "models/cube.gltf#Scene0")]
    pub cube: Handle<Scene>,
    pub placement_materials: PlacementMaterials,
}

impl BuildingAssets {
    pub fn get_by_building(&self, building: &Building) -> Handle<Scene> {
        match building {
            Building::Batteries { .. } => self.batteries.clone_weak(),
            Building::CoalPlant => self.coal_plant.clone_weak(),
            Building::GasPlant => self.gas_plant.clone_weak(),
            Building::SolarArray => self.solar_array.clone_weak(),
            Building::WindTurbines => self.wind_turbines.clone_weak(),
        }
    }
}

pub struct PlacementMaterials {
    pub valid_material: Handle<StandardMaterial>,
    pub invalid_material: Handle<StandardMaterial>,
}

impl FromWorld for PlacementMaterials {
    fn from_world(world: &mut World) -> Self {
        let mut materials = world
            .get_resource_mut::<Assets<StandardMaterial>>()
            .unwrap();
        Self {
            valid_material: materials.add(StandardMaterial {
                base_color: Color::GREEN.with_a(0.75),
                cull_mode: None,
                ..default()
            }),
            invalid_material: materials.add(StandardMaterial {
                base_color: Color::RED.with_a(0.75),
                cull_mode: None,
                ..default()
            }),
        }
    }
}

#[derive(Resource)]
pub struct BuildingThumbnails {
    pub batteries: Handle<Image>,
    pub batteries_id: TextureId,
    pub coal: Handle<Image>,
    pub coal_id: TextureId,
    pub gas: Handle<Image>,
    pub gas_id: TextureId,
    pub solar: Handle<Image>,
    pub solar_id: TextureId,
    pub wind: Handle<Image>,
    pub wind_id: TextureId,
}

#[derive(Resource)]
pub struct Images {
    pub main_menu: Handle<Image>,
    pub main_menu_id: TextureId,
}

impl BuildingThumbnails {
    pub fn get_by_building(&self, building: Building) -> Handle<Image> {
        match building {
            Building::Batteries { .. } => self.batteries.clone_weak(),
            Building::CoalPlant => self.coal.clone_weak(),
            Building::GasPlant => self.gas.clone_weak(),
            Building::SolarArray => self.solar.clone_weak(),
            Building::WindTurbines => self.wind.clone_weak(),
        }
    }
    pub fn get_id_by_building(&self, building: Building) -> TextureId {
        match building {
            Building::Batteries { .. } => self.batteries_id,
            Building::CoalPlant => self.coal_id,
            Building::GasPlant => self.gas_id,
            Building::SolarArray => self.solar_id,
            Building::WindTurbines => self.wind_id,
        }
    }
}

impl FromWorld for BuildingThumbnails {
    fn from_world(world: &mut World) -> Self {
        let asset_server = world.get_resource::<AssetServer>().unwrap();
        let batteries: Handle<Image> = asset_server.load("thumbnails/battery.png");
        let coal: Handle<Image> = asset_server.load("thumbnails/coal_plant.png");
        let gas: Handle<Image> = asset_server.load("thumbnails/gas_plant.png");
        let solar: Handle<Image> = asset_server.load("thumbnails/solar_array.png");
        let wind: Handle<Image> = asset_server.load("thumbnails/wind_turbine.png");

        let mut egui_user_textures = world.get_resource_mut::<EguiUserTextures>().unwrap();
        let batteries_id: TextureId = egui_user_textures.add_image(batteries.clone_weak());
        let coal_id: TextureId = egui_user_textures.add_image(coal.clone_weak());
        let gas_id: TextureId = egui_user_textures.add_image(gas.clone_weak());
        let solar_id: TextureId = egui_user_textures.add_image(solar.clone_weak());
        let wind_id: TextureId = egui_user_textures.add_image(wind.clone_weak());

        Self {
            batteries,
            batteries_id,
            coal,
            coal_id,
            gas,
            gas_id,
            solar,
            solar_id,
            wind,
            wind_id,
        }
    }
}

impl FromWorld for Images {
    fn from_world(world: &mut World) -> Self {
        let asset_server = world.get_resource::<AssetServer>().unwrap();
        let main_menu = asset_server.load("images/main_menu.png");
        let mut egui_user_textures = world.get_resource_mut::<EguiUserTextures>().unwrap();
        let main_menu_id = egui_user_textures.add_image(main_menu.clone_weak());
        Self {
            main_menu,
            main_menu_id,
        }
    }
}

#[derive(Resource)]
pub struct TerrainAssets {
    pub water: Handle<StandardMaterial>,
    pub water_disabled: Handle<StandardMaterial>,
    pub grass: Handle<StandardMaterial>,
    pub grass_disabled: Handle<StandardMaterial>,
    pub mountain: Handle<StandardMaterial>,
    pub mountain_disabled: Handle<StandardMaterial>,
}

impl TerrainAssets {
    pub fn get_material(&self, terrain: TerrainTile) -> Handle<StandardMaterial> {
        match terrain.terrain_type {
            TerrainType::Water => {
                if terrain.enabled {
                    self.water.clone_weak()
                } else {
                    self.water_disabled.clone_weak()
                }
            }
            TerrainType::Grass => {
                if terrain.enabled {
                    self.grass.clone_weak()
                } else {
                    self.grass_disabled.clone_weak()
                }
            }
            TerrainType::Mountain => {
                if terrain.enabled {
                    self.mountain.clone_weak()
                } else {
                    self.mountain_disabled.clone_weak()
                }
            }
        }
    }
}

impl FromWorld for TerrainAssets {
    fn from_world(world: &mut World) -> Self {
        let mut materials = world
            .get_resource_mut::<Assets<StandardMaterial>>()
            .unwrap();

        let alpha_mode = AlphaMode::Add;
        Self {
            water: materials.add(StandardMaterial {
                base_color: Color::BLUE,
                ..default()
            }),
            water_disabled: materials.add(StandardMaterial {
                base_color: Color::BLUE.with_a(0.5),
                alpha_mode,
                ..default()
            }),
            grass: materials.add(StandardMaterial {
                base_color: Color::DARK_GREEN,
                ..default()
            }),
            grass_disabled: materials.add(StandardMaterial {
                base_color: Color::DARK_GREEN.with_a(0.5),
                alpha_mode,
                ..default()
            }),
            mountain: materials.add(StandardMaterial {
                base_color: Color::GRAY,
                ..default()
            }),
            mountain_disabled: materials.add(StandardMaterial {
                base_color: Color::GRAY.with_a(0.5),
                alpha_mode,
                ..default()
            }),
        }
    }
}

#[derive(Resource)]
pub struct DocsAssets {
    pub tutorial_text: String,
    pub common_mark_cache: CommonMarkCache,
}

impl Default for DocsAssets {
    fn default() -> Self {
        Self {
            tutorial_text: include_str!("../docs/tutorial.md").to_string(),
            common_mark_cache: CommonMarkCache::default(),
        }
    }
}
