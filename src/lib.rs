#![allow(clippy::too_many_arguments, clippy::type_complexity)]

use bevy::diagnostic::{FrameTimeDiagnosticsPlugin, LogDiagnosticsPlugin};
use bevy::prelude::*;
#[cfg(not(target_arch = "wasm32"))]
use bevy_atmosphere::plugin::AtmosphereCamera;
use bevy_dolly::dolly_type::Rig;
use bevy_dolly::prelude::{Arm, LookAt, Position, YawPitch};
use bevy_mod_raycast::{RaycastMesh, RaycastSource};

use crate::assets::{AssetLoadingPlugin, TerrainAssets};
use crate::camera::atmosphere::{DayLength, Sun};
use crate::camera::camera_controller::{CameraSettings, MainCamera};
use crate::camera::CameraControlPlugin;
use crate::core::build_selection::BuildSelection;
use crate::core::building_selection::BuildingSelectionPlugin;
use crate::core::months::{
    FinalMonth, LastMonthStatistics, LossTimer, MonthTimer, MonthsUnsuccessful, ProductionTargets,
};
use crate::core::placement::{PlacementPlugin, TerrainRaycastSet};
use crate::core::research::shape::Cube;
use crate::core::research::{GridUpgrades, ResearchPlugin, ResearchTree};
use crate::core::resources::Resources;
use crate::core::shipping_options::ShippingOption;
use crate::core::states::{DespawnInMenu, GameState};
use crate::core::terrain_grid::terrain::{GameTerrain, TerrainObject};
use crate::core::time_scale::{IngameTime, TimeScale};
use crate::core::upgrades::UpgradePlugin;
use crate::ui::info_windows::building_selection::BuildingSelectionWindow;
use crate::ui::info_windows::current_building::CurrentBuildingWindow;
use crate::ui::info_windows::month_end::MonthEndWindow;
use crate::ui::info_windows::plots::PlotsWindow;
use crate::ui::info_windows::research::ResearchWindow;
use crate::ui::info_windows::shipping::ShippingWindow;
use crate::ui::info_windows::{HoveringAboveWindow, LastCursorPosition, WindowsOpen};
use crate::ui::pause_menu::PauseMenuWindow;
use crate::ui::ui_plugin::UIPlugin;

pub mod assets;
pub mod camera;
pub mod core;
pub mod ui;

macro_rules! reinit_resources {
    ($commands:ident, $t:ty) => {
        $commands.remove_resource::<$t>();
        $commands.init_resource::<$t>();
    };
    ($commands:ident, $t:ty, $($rest:ty), +) => {
        $commands.remove_resource::<$t>();
        $commands.init_resource::<$t>();
        reinit_resources!($commands, $($rest),+);
    };
}

pub struct GamePlugin;

impl Plugin for GamePlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(AmbientLight {
            brightness: 0.08,
            ..default()
        })
        .init_resource::<AudioPlaying>()
        .add_plugins(DefaultPlugins.set(WindowPlugin {
            primary_window: Some(Window {
                fit_canvas_to_parent: true,
                ..default()
            }),
            ..default()
        }))
        .add_plugin(FrameTimeDiagnosticsPlugin)
        .add_plugin(LogDiagnosticsPlugin::default())
        .add_plugin(PlacementPlugin)
        .add_plugin(AssetLoadingPlugin)
        .add_plugin(UIPlugin)
        .add_plugin(CameraControlPlugin)
        .add_plugin(ResearchPlugin)
        .add_plugin(BuildingSelectionPlugin)
        .add_plugin(UpgradePlugin)
        .add_systems(
            (
                atmosphere_setup,
                setup_camera,
                setup_game,
                setup_terrain,
                start_game,
                setup_audio.run_if(resource_equals(AudioPlaying(false))),
            )
                .chain()
                .in_schedule(OnEnter(GameState::PreGame)),
        );
    }
}

fn start_game(mut next_state: ResMut<NextState<GameState>>) {
    next_state.set(GameState::InGame)
}

fn setup_game(mut commands: Commands) {
    reinit_resources!(
        commands,
        DayLength,
        CameraSettings,
        BuildSelection,
        FinalMonth,
        MonthTimer,
        ProductionTargets,
        LastMonthStatistics,
        LossTimer,
        MonthsUnsuccessful,
        ResearchTree,
        GridUpgrades,
        Resources,
        ShippingOption,
        TimeScale,
        IngameTime,
        GameTerrain,
        PauseMenuWindow,
        ShippingWindow,
        PlotsWindow,
        BuildingSelectionWindow,
        HoveringAboveWindow,
        WindowsOpen,
        LastCursorPosition,
        MonthEndWindow,
        ResearchWindow,
        CurrentBuildingWindow
    );
}

fn setup_camera(mut commands: Commands) {
    commands
        .spawn((
            Camera3dBundle {
                transform: Transform::from_xyz(0.0, 140.0, 90.0).looking_at(Vec3::ZERO, Vec3::Y),
                ..default()
            },
            Rig::builder()
                .with(Position::new(Vec3::new(2.5 * 5.0, 0.0, 2.5 * 5.0)))
                .with(YawPitch::new().pitch_degrees(25.0).yaw_degrees(45.0))
                .with(Arm::new(-75.0 * Vec3::Z))
                .with(LookAt::new(Vec3::ZERO))
                .build(),
            #[cfg(not(target_arch = "wasm32"))]
            AtmosphereCamera::default(),
        ))
        .insert(RaycastSource::<TerrainRaycastSet>::new_transform_empty())
        .insert(DespawnInMenu)
        .insert(MainCamera);
}

fn setup_terrain(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    terrain_assets: Res<TerrainAssets>,
    terrain: Res<GameTerrain>,
) {
    let (grid_size_z, grid_size_x) = (terrain.terrain_grid.rows(), terrain.terrain_grid.cols());

    for x in 0..grid_size_x {
        for z in 0..grid_size_z {
            let tile = terrain.terrain_grid[z][x];
            let terrain_tile = tile.terrain_tile;
            let thickness = 15.0;
            commands
                .spawn(PbrBundle {
                    mesh: meshes.add(Mesh::from(Cube { size: 1.0 })),
                    material: terrain_assets.get_material(terrain_tile),
                    transform: Transform::from_scale(Vec3::new(
                        terrain.tile_size,
                        thickness,
                        terrain.tile_size,
                    ))
                    .with_translation(
                        terrain.grid_to_world((x, z)) + Vec3::NEG_Y * thickness * 0.5,
                    ),
                    ..default()
                })
                .insert(TerrainObject(x, z))
                .insert(RaycastMesh::<TerrainRaycastSet>::default())
                .insert(DespawnInMenu);
        }
    }
}

fn atmosphere_setup(mut commands: Commands) {
    commands
        .spawn(DirectionalLightBundle {
            directional_light: DirectionalLight {
                shadows_enabled: true,
                ..default()
            },
            ..default()
        })
        .insert(DespawnInMenu)
        .insert(Sun);
}

#[derive(Default, Resource, PartialEq, Eq)]
struct AudioPlaying(bool);

fn setup_audio(
    asset_server: Res<AssetServer>,
    audio: Res<Audio>,
    mut audio_playing: ResMut<AudioPlaying>,
) {
    audio_playing.0 = true;

    let settings = PlaybackSettings {
        volume: 0.3,
        ..PlaybackSettings::LOOP
    };

    audio.play_with_settings(asset_server.load("audio/music.flac"), settings);
}
