use std::fmt::{Display, Formatter};

use bevy::prelude::*;
use quantities::duration::SECOND;
use strum_macros::EnumIter;

use crate::core::states::GameState;

pub struct TimeScalePlugin;

impl Plugin for TimeScalePlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<TimeScale>()
            .init_resource::<IngameTime>()
            .add_system(set_time_scale.in_set(OnUpdate(GameState::InGame)));
    }
}

#[derive(Clone, EnumIter, Eq, PartialEq, Resource)]
pub enum TimeScale {
    Paused(Box<TimeScale>),
    Slow,
    Normal,
    Fast,
}

impl Default for TimeScale {
    fn default() -> Self {
        Self::Paused(Box::new(Self::Normal))
    }
}

impl Display for TimeScale {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            TimeScale::Paused(_) => write!(f, "\u{23f8}"),
            TimeScale::Slow => write!(f, "\u{23f5}"),
            TimeScale::Normal => write!(f, "\u{23f5}\u{23f5}"),
            TimeScale::Fast => write!(f, "\u{23f5}\u{23f5}\u{23f5}"),
        }
    }
}

impl TimeScale {
    pub fn convert_delta_time(&self, time: &Time) -> quantities::duration::Duration {
        self.as_f64() * time.delta_seconds_f64() * SECOND
    }

    pub fn as_f64(&self) -> f64 {
        match self {
            TimeScale::Paused(_) => 0.0,
            TimeScale::Slow => 20000.0,
            TimeScale::Normal => 86400.0,
            TimeScale::Fast => 500000.0,
        }
    }

    pub fn as_f32(&self) -> f32 {
        match self {
            TimeScale::Paused(_) => 0.0,
            TimeScale::Slow => 2000.0,
            TimeScale::Normal => 86400.0,
            TimeScale::Fast => 500000.0,
        }
    }

    pub fn button_tooltip(&self) -> String {
        match self {
            TimeScale::Paused(_) => "Pause".to_string(),
            TimeScale::Slow => "Slow".to_string(),
            TimeScale::Normal => "Normal Speed".to_string(),
            TimeScale::Fast => "Faster".to_string(),
        }
    }

    pub fn speed_up(&mut self) {
        *self = match self {
            TimeScale::Paused(_) => TimeScale::Slow,
            TimeScale::Slow => TimeScale::Normal,
            TimeScale::Normal => TimeScale::Fast,
            TimeScale::Fast => TimeScale::Fast,
        }
    }

    pub fn speed_down(&mut self) {
        *self = match self {
            TimeScale::Paused(t) => TimeScale::Paused(t.clone()),
            TimeScale::Slow => TimeScale::Paused(Box::new(TimeScale::Slow)),
            TimeScale::Normal => TimeScale::Slow,
            TimeScale::Fast => TimeScale::Normal,
        }
    }

    pub fn toggle_pause(&mut self) {
        if let TimeScale::Paused(t_boxed) = self {
            *self = *t_boxed.clone();
            return;
        }
        *self = TimeScale::Paused(Box::new(self.clone()))
    }

    pub fn reset(&mut self) {
        if let TimeScale::Paused(_) = self {
            return;
        }
        *self = TimeScale::Normal
    }
}

#[derive(Resource, Default, Deref, DerefMut, Copy, Clone)]
pub struct IngameTime(pub f64);

fn set_time_scale(keys: Res<Input<KeyCode>>, mut time_scale: ResMut<TimeScale>) {
    if keys.just_pressed(KeyCode::Period) {
        time_scale.speed_up();
    }
    if keys.just_pressed(KeyCode::Comma) {
        time_scale.speed_down();
    }
    if keys.just_pressed(KeyCode::Space) {
        time_scale.toggle_pause();
    }
    if keys.just_pressed(KeyCode::Minus) {
        time_scale.reset();
    }
}
