use bevy::prelude::*;
use bevy_egui::egui::plot::PlotPoint;
use quantities::duration::{Duration, MONTH};
use quantities::mass::Mass;
use quantities::power::Power;
use quantities::{HasRefUnit, Quantity};
use strum::IntoEnumIterator;

use crate::core::months::{FinalMonth, ProductionTargets};
use crate::core::resources::{update_resources, Resources};
use crate::core::states::GameState;
use crate::core::time_scale::IngameTime;
use crate::ui::info_windows::plots::PlotPanel;

pub struct StatisticsPlugin;

impl Plugin for StatisticsPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(FixedTime::new_from_secs(1.0 / 60.0))
            .add_system(setup_statistics.in_schedule(OnEnter(GameState::PreGame)))
            .add_system(
                update_statistics
                    .run_if(resource_exists::<Statistics>())
                    .after(update_resources)
                    .in_schedule(CoreSchedule::FixedUpdate)
                    .in_set(OnUpdate(GameState::InGame)),
            );
    }
}

#[derive(Resource)]
pub struct Statistics {
    pub co2_targets: Vec<PlotPoint>,
    pub co2_emissions: Vec<PlotPoint>,
    pub power_targets: Vec<PlotPoint>,
    pub power_production: Vec<PlotPoint>,
    pub energy_stored: Vec<PlotPoint>,
    pub money: Vec<PlotPoint>,
    pub reputation: Vec<PlotPoint>,
}

fn setup_statistics(
    mut commands: Commands,
    final_month: Res<FinalMonth>,
    production_targets: Res<ProductionTargets>,
) {
    let mut co2_targets = Vec::with_capacity(final_month.0 * 2);
    let mut power_targets = Vec::with_capacity(final_month.0 * 2);

    for month in 0..final_month.0 {
        let t = month as f64;
        let time = Duration::new(t, MONTH);

        let co2_point = PlotPoint::new(
            t,
            production_targets
                .co2_target(time)
                .equiv_amount(Mass::REF_UNIT),
        );
        let power_point = PlotPoint::new(
            t,
            production_targets
                .power_target(time)
                .equiv_amount(Power::REF_UNIT),
        );

        co2_targets
            .extend_from_slice(&[co2_point, PlotPoint::new((month + 1) as f64, co2_point.y)]);
        power_targets.extend_from_slice(&[
            power_point,
            PlotPoint::new((month + 1) as f64, power_point.y),
        ]);
    }

    let statistics = Statistics {
        co2_targets,
        co2_emissions: Vec::new(),
        power_targets,
        power_production: Vec::new(),
        energy_stored: Vec::new(),
        money: Vec::new(),
        reputation: Vec::new(),
    };

    commands.insert_resource(statistics);
}

fn update_statistics(
    ingame_time: Res<IngameTime>,
    resources: Res<Resources>,
    mut statistics: ResMut<Statistics>,
) {
    for panel in PlotPanel::iter() {
        panel.push_statistic(&ingame_time, &resources, &mut statistics);
    }
}
