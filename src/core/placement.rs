use bevy::prelude::*;
use bevy_mod_raycast::{
    DefaultRaycastingPlugin, Intersection, RaycastMesh, RaycastMethod, RaycastSource, RaycastSystem,
};

use crate::assets::BuildingAssets;
use crate::core::build_selection::{BuildSelection, BuildSelectionPlugin};
use crate::core::buildings::Building;
use crate::core::resources::Resources;
use crate::core::shipping_options::ShippingOption;
use crate::core::states::{DespawnInMenu, GameState};
use crate::core::terrain_grid::terrain::{GameTerrain, TerrainPlugin};
use crate::ui::info_windows::hovers_window;
use crate::ui::ui_plugin::UiSystemSet;

pub struct PlacementPlugin;

impl Plugin for PlacementPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugin(DefaultRaycastingPlugin::<TerrainRaycastSet>::default())
            .add_plugin(TerrainPlugin)
            .add_plugin(BuildSelectionPlugin)
            .add_system(
                update_raycast_with_cursor
                    .in_base_set(CoreSet::First)
                    .before(RaycastSystem::BuildRays::<TerrainRaycastSet>),
            )
            .add_systems(
                (
                    insert_building_material,
                    display_placement_marker,
                    place_building.run_if(not(hovers_window)),
                    insert_raycast_mesh_to_children,
                )
                    .in_set(PlacementSet),
            )
            .configure_set(
                PlacementSet
                    .in_set(OnUpdate(GameState::InGame))
                    .after(UiSystemSet::InGame),
            );
    }
}

#[derive(SystemSet, Debug, Hash, PartialEq, Eq, Clone)]
pub struct PlacementSet;

#[derive(Clone, Reflect)]
pub struct TerrainRaycastSet;

#[derive(Component, Default)]
pub struct PlacementMarker {
    pub building: Building,
}

#[derive(Component)]
pub struct PlacementMaterial;

#[derive(Component)]
pub struct InsertRaycastMeshToChildren;

fn insert_raycast_mesh_to_children(
    query: Query<Entity, Added<InsertRaycastMeshToChildren>>,
    children_query: Query<&Children>,
    mut commands: Commands,
) {
    for entity in &query {
        for entity in children_query.iter_descendants(entity) {
            commands
                .entity(entity)
                .insert(RaycastMesh::<TerrainRaycastSet>::default());
        }
    }
}

fn update_raycast_with_cursor(
    mut cursor: EventReader<CursorMoved>,
    mut query: Query<
        &mut RaycastSource<TerrainRaycastSet>,
        Changed<RaycastSource<TerrainRaycastSet>>,
    >,
) {
    let Some(cursor_position) = cursor.iter().last() else {
        return;
    };

    for mut raycast_source in query.iter_mut() {
        raycast_source.cast_method = RaycastMethod::Screenspace(cursor_position.position);
    }
}

pub fn insert_building_material(
    mut commands: Commands,
    placement_marker_query: Query<Entity, (With<PlacementMarker>, Without<PlacementMaterial>)>,
    children: Query<&Children>,
    building_assets: Res<BuildingAssets>,
) {
    let Ok(entity) = placement_marker_query.get_single() else {
        return;
    };

    for child in children.iter_descendants(entity) {
        commands.entity(child).insert(
            building_assets
                .placement_materials
                .valid_material
                .clone_weak(),
        );

        commands.entity(entity).insert(PlacementMaterial);
    }
}

#[allow(clippy::too_many_arguments)]
pub fn display_placement_marker(
    mut commands: Commands,
    mut marker_query: Query<(&mut Transform, &mut Visibility), With<PlacementMarker>>,
    material_query: Query<Entity, With<PlacementMaterial>>,
    intersection_query: Query<&Intersection<TerrainRaycastSet>>,
    children: Query<&Children>,
    building_assets: Res<BuildingAssets>,
    game_grid: Res<GameTerrain>,
    selection: Res<BuildSelection>,
    resources: Res<Resources>,
    shipping_option: Res<ShippingOption>,
) {
    if *selection == BuildSelection::None {
        return;
    };

    let Ok((mut transform, mut visibility)) = marker_query.get_single_mut() else {
        return;
    };

    if let Ok(intersection) = intersection_query.get_single() {
        match intersection.position() {
            None => *visibility = Visibility::Hidden,
            Some(&position) => {
                let grid_position = game_grid.world_to_grid(position);
                transform.translation = game_grid.grid_to_world(grid_position);
                *visibility = Visibility::Visible;

                let terrain_type = game_grid.terrain_grid[grid_position.1][grid_position.0]
                    .terrain_tile
                    .terrain_type;
                let mut can_build = false;
                if let BuildSelection::Building(building) = *selection {
                    can_build = building.buildable_terrain().contains(&terrain_type);
                }

                let material = if game_grid.has_entry(grid_position)
                    || !game_grid.is_enabled(grid_position)
                    || !can_build
                    || resources.money < selection.unwrap().price(&shipping_option)
                {
                    building_assets
                        .placement_materials
                        .invalid_material
                        .clone_weak()
                } else {
                    building_assets
                        .placement_materials
                        .valid_material
                        .clone_weak()
                };

                for child in material_query
                    .iter()
                    .flat_map(|e| children.iter_descendants(e))
                {
                    commands.entity(child).insert(material.clone_weak());
                }
            }
        }
    }
}

pub fn place_building(
    mut commands: Commands,
    intersection_query: Query<&Intersection<TerrainRaycastSet>>,
    mouse_button_input: Res<Input<MouseButton>>,
    building_assets: Res<BuildingAssets>,
    building_selection: Res<BuildSelection>,
    mut game_grid: ResMut<GameTerrain>,
    mut resources: ResMut<Resources>,
    shipping_option: Res<ShippingOption>,
) {
    if building_selection.is_none() || !mouse_button_input.pressed(MouseButton::Left) {
        return;
    }

    let Ok(intersection) = intersection_query.get_single() else {
        return;
    };

    if let Some(&intersection_position) = intersection.position() {
        let tile_size = game_grid.tile_size;
        let grid_position = game_grid.world_to_grid(intersection_position);

        // Basically intersection_position but with the right height, dunno if needed.
        let position = game_grid.grid_to_world(grid_position);
        let building = building_selection.unwrap();

        let terrain_type = game_grid.terrain_grid[grid_position.1][grid_position.0]
            .terrain_tile
            .terrain_type;

        let can_build = building.buildable_terrain().contains(&terrain_type);

        if !game_grid.has_entry(grid_position)
            && resources.money >= building.price(&shipping_option)
            && game_grid.is_enabled(grid_position)
            && can_build
        {
            game_grid.insert(
                intersection_position,
                commands
                    .spawn(SceneBundle {
                        scene: building_selection
                            .get_handle(building_assets.into_inner())
                            .unwrap(),
                        transform: Transform::from_translation(position)
                            .with_scale(Vec3::splat(tile_size)),
                        ..default()
                    })
                    .insert(DespawnInMenu)
                    .insert(*building)
                    .insert(InsertRaycastMeshToChildren)
                    .id(),
                *building,
            );
            resources.add(building, &(*shipping_option).into());
        }
    }
}
