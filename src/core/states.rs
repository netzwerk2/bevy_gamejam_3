use bevy::prelude::*;

use crate::core::resources::ResourcePlugin;

#[derive(Clone, Copy, Debug, Default, Eq, PartialEq, Hash, States)]
pub enum GameState {
    #[default]
    Loading,
    Menu,
    Tutorial,
    PreGame,
    InGame,
    Paused,
    MonthEnd,
    GameLost,
    GameWon,
}

pub struct StatePlugin;

impl Plugin for StatePlugin {
    fn build(&self, app: &mut App) {
        app.add_plugin(ResourcePlugin)
            .add_state::<GameState>()
            .add_system(despawn_in_menu.in_schedule(OnEnter(GameState::Menu)))
            .add_system(despawn_in_menu.in_schedule(OnEnter(GameState::GameLost)));
    }
}

#[derive(Component)]
pub struct DespawnInMenu;

fn despawn_in_menu(mut commands: Commands, query: Query<Entity, With<DespawnInMenu>>) {
    for entity in &query {
        commands.entity(entity).despawn_recursive();
    }
}
