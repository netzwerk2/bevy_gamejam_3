use std::fmt::{Display, Formatter};

use bevy::prelude::*;
use strum_macros::EnumIter;

pub struct ShippingPlugin;

impl Plugin for ShippingPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<ShippingOption>();
    }
}

#[derive(Copy, Clone, Default, EnumIter, Eq, PartialEq, Resource)]
pub enum ShippingOption {
    Bicycle,
    ECar,
    BigTruck,
    #[default]
    Standard,
}

impl ShippingOption {
    pub fn co2_multiplier(&self) -> f64 {
        match self {
            ShippingOption::Bicycle => 0.8,
            ShippingOption::ECar => 0.95,
            ShippingOption::BigTruck => 1.3,
            ShippingOption::Standard => 1.0,
        }
    }

    pub fn money_multiplier(&self) -> f64 {
        match self {
            ShippingOption::Bicycle => 1.5,
            ShippingOption::ECar => 1.3,
            ShippingOption::BigTruck => 0.95,
            ShippingOption::Standard => 1.0,
        }
    }

    pub fn tooltip(&self) -> String {
        match self {
            ShippingOption::Bicycle => format!(
                "Ship all the building materials with bicycle couriers.\n{}",
                self.modifiers_tooltip()
            ),
            ShippingOption::ECar => format!(
                "Ship building materials with electric cars.\n{}",
                self.modifiers_tooltip()
            ),
            ShippingOption::BigTruck => format!(
                "Ship all building materials in one big truck.\n{}",
                self.modifiers_tooltip()
            ),
            ShippingOption::Standard => format!(
                "The default shipping method for major power plants.\n{}",
                self.modifiers_tooltip()
            ),
        }
    }

    fn modifiers_tooltip(&self) -> String {
        let percentage_money = (self.money_multiplier() - 1.0) * 100.0;
        let percentage_co2 = (self.co2_multiplier() - 1.0) * 100.0;

        format!(
            "Costs: {:.1}%, Building Emissions: {:.1}%",
            percentage_money, percentage_co2
        )
    }
}

impl Display for ShippingOption {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            ShippingOption::Bicycle => write!(f, "Bicycles"),
            ShippingOption::ECar => write!(f, "Electric Cars"),
            ShippingOption::BigTruck => write!(f, "Big Truck"),
            ShippingOption::Standard => write!(f, "Standard"),
        }
    }
}
