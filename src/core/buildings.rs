use std::fmt::{Display, Formatter};

use bevy::prelude::*;
use noisy_bevy::simplex_noise_3d;
use quantities::currency::{Currency, MILLION_EURO};
use quantities::energy::{Energy, GIGAWATT_HOUR, WATT_HOUR};
use quantities::mass::{Mass, KILOTONNE, TONNE};
use quantities::mass_flow_rate::{MassFlowRate, KILOTONNE_PER_MONTH, TONNE_PER_MONTH};
use quantities::power::{Power, MEGAWATT};
use strum_macros::EnumIter;

use crate::core::shipping_options::ShippingOption;
use crate::core::terrain_grid::terrain::{TerrainTile, TerrainType};
use crate::core::upgrades::{Upgrade, UpgradeStage};
use crate::ui::QuantityExt;

#[derive(Copy, Clone, Component, Debug, Default, Eq, PartialEq, EnumIter)]
pub enum Building {
    Batteries {
        energy: Energy,
    },
    #[default]
    CoalPlant,
    GasPlant,
    SolarArray,
    WindTurbines,
}

impl Building {
    pub fn price(&self, shipping_option: &ShippingOption) -> Currency {
        let shipping_multiplier = shipping_option.money_multiplier();

        match self {
            Building::Batteries { .. } => 100.0 * MILLION_EURO * shipping_multiplier,
            Building::CoalPlant => 300.0 * MILLION_EURO * shipping_multiplier,
            Building::GasPlant => 450.0 * MILLION_EURO * shipping_multiplier,
            Building::SolarArray => 60.0 * MILLION_EURO * shipping_multiplier,
            Building::WindTurbines => 100.0 * MILLION_EURO * shipping_multiplier,
        }
    }

    pub fn destroy_price(&self, shipping_option: &ShippingOption) -> Currency {
        let shipping_multiplier = shipping_option.money_multiplier();

        match self {
            Building::Batteries { .. } => 25.0 * MILLION_EURO * shipping_multiplier,
            Building::CoalPlant => 75.0 * MILLION_EURO * shipping_multiplier,
            Building::GasPlant => 100.0 * MILLION_EURO * shipping_multiplier,
            Building::SolarArray => 15.0 * MILLION_EURO * shipping_multiplier,
            Building::WindTurbines => 25.0 * MILLION_EURO * shipping_multiplier,
        }
    }

    pub fn build_emissions(&self, shipping_option: &ShippingOption) -> Mass {
        let shipping_multiplier = shipping_option.co2_multiplier();

        match self {
            Building::Batteries { .. } => 500.0 * TONNE * shipping_multiplier,
            Building::CoalPlant => 5.625 * KILOTONNE * shipping_multiplier,
            Building::GasPlant => 6.5 * KILOTONNE * shipping_multiplier,
            Building::SolarArray => 75.0 * TONNE * shipping_multiplier,
            Building::WindTurbines => 750.0 * TONNE * shipping_multiplier,
        }
    }

    pub fn destroy_emissions(&self, shipping_option: &ShippingOption) -> Mass {
        self.build_emissions(shipping_option) * 0.5
    }

    pub fn emissions(&self, upgrade: Option<&UpgradeStage>) -> MassFlowRate {
        match self {
            Building::Batteries { .. } => 2.0 * TONNE_PER_MONTH,
            Building::CoalPlant => 2.0 * KILOTONNE_PER_MONTH * upgrade.multiplier().co2,
            Building::GasPlant => 1.5 * KILOTONNE_PER_MONTH * upgrade.multiplier().co2,
            Building::SolarArray => 0.01 * TONNE_PER_MONTH,
            Building::WindTurbines => 0.01 * TONNE_PER_MONTH,
        }
    }

    pub fn power(
        &self,
        position: Vec3,
        terrain: TerrainTile,
        time: f32,
        illuminance: f64,
        upgrade: Option<&UpgradeStage>,
    ) -> Power {
        match self {
            Building::SolarArray => self.max_power(upgrade) * illuminance * 1e-5,
            Building::WindTurbines => {
                let mut x = ((simplex_noise_3d(Vec3::new(position.x, position.y, time)) as f64
                    + 1.0)
                    * 0.5)
                    .clamp(0.0, 1.0);
                x *= terrain.terrain_multiplier_wind();
                self.max_power(upgrade) * x
            }
            _ => self.max_power(upgrade),
        }
    }

    pub fn max_power(&self, upgrade: Option<&UpgradeStage>) -> Power {
        match self {
            Building::Batteries { .. } => 12.0 * MEGAWATT * upgrade.multiplier().power,
            Building::CoalPlant => 30.0 * MEGAWATT,
            Building::GasPlant => 32.0 * MEGAWATT * upgrade.multiplier().co2,
            Building::SolarArray => 12.0 * MEGAWATT * upgrade.multiplier().power,
            Building::WindTurbines => 12.0 * MEGAWATT * upgrade.multiplier().power,
        }
    }

    pub fn max_energy(&self, upgrade: Option<&UpgradeStage>) -> Energy {
        match self {
            Building::Batteries { .. } => 3.0 * GIGAWATT_HOUR * upgrade.multiplier().power,
            Building::CoalPlant => 0.0 * WATT_HOUR,
            Building::SolarArray => 0.0 * WATT_HOUR,
            Building::WindTurbines => 0.0 * WATT_HOUR,
            Building::GasPlant => 0.0 * WATT_HOUR,
        }
    }

    pub fn buildable_terrain(&self) -> Vec<TerrainType> {
        match self {
            Building::Batteries { .. } => vec![TerrainType::Grass, TerrainType::Mountain],
            Building::CoalPlant => vec![TerrainType::Grass, TerrainType::Mountain],
            Building::GasPlant => vec![TerrainType::Grass, TerrainType::Mountain],
            Building::SolarArray => vec![TerrainType::Grass, TerrainType::Mountain],
            Building::WindTurbines => vec![
                TerrainType::Water,
                TerrainType::Grass,
                TerrainType::Mountain,
            ],
        }
    }

    pub fn upgrade_tooltip(&self, upgrade: Option<&UpgradeStage>) -> String {
        let shipping_option = ShippingOption::default();

        match upgrade {
            Some(&UpgradeStage::Third) => "You already upgraded to the max level.".to_string(),
            _ => match self {
                Building::Batteries { .. } => {
                    format!(
                        "Power: +{:.1}%, Energy: +{:.1}%, Costs: {}",
                        (upgrade.next().multiplier().power - 1.0) * 100.0,
                        (upgrade.next().multiplier().power - 1.0) * 100.0,
                        upgrade.costs() * self.price(&shipping_option)
                    )
                }
                Building::CoalPlant | Building::GasPlant => {
                    format!(
                        "Emissions: -{:.1}%, Costs: {}",
                        (1.0 - upgrade.next().multiplier().co2) * 100.0,
                        upgrade.costs() * self.price(&shipping_option)
                    )
                }
                Building::SolarArray | Building::WindTurbines => {
                    format!(
                        "Power: +{:.1}%, Costs: {}",
                        (upgrade.next().multiplier().power - 1.0) * 100.0,
                        upgrade.costs() * self.price(&shipping_option)
                    )
                }
            },
        }
    }

    pub fn tool_tip(&self, shipping_option: &ShippingOption) -> String {
        match self {
            Building::Batteries { .. } => {
                format!(
                    "Batteries store surplus energy.\nBuilding Costs: {}\nBuilding Emissions: {}\nCharge/Discharge Power: {}\nStorable energy: {}",
                    self.price(shipping_option).to_formatted_string(),
                    self.build_emissions(shipping_option).to_formatted_string(),
                    self.max_power(None).to_formatted_string(),
                    self.max_energy(None).to_formatted_string()
                )
            }
            Building::CoalPlant => {
                format!(
                    "Coal plants serve cheap dirty energy.\nBuilding Costs: {}\nBuilding Emissions: {}\nEmissions: {}\nPower Generation: {}",
                    self.price(shipping_option).to_formatted_string(),
                    self.build_emissions(shipping_option).to_formatted_string(),
                    self.emissions(None).to_formatted_string(),
                    self.max_power(None).to_formatted_string()
                )
            }
            Building::GasPlant => {
                format!("Gas Plants are similar to Coal plants but emit less co2.\nBuilding Costs: {}\nBuilding Emissions: {}\nEmissions: {}\nPower Generation: {}",
                        self.price(shipping_option).to_formatted_string(),
                        self.build_emissions(shipping_option).to_formatted_string(),
                        self.emissions(None).to_formatted_string(),
                        self.max_power(None).to_formatted_string()
                )
            }
            Building::SolarArray => {
                format!(
                    "A very little bit of green energy.\nBuilding Costs: {}\nBuilding Emissions: {}\nPower Generation: {}",
                    self.price(shipping_option).to_formatted_string(),
                    self.build_emissions(shipping_option).to_formatted_string(),
                    self.max_power(None).to_formatted_string()
                )
            }
            Building::WindTurbines => {
                format!(
                    "A bit more of green energy.\nProduces 20 % more energy when placed offshore or 50% more when placed high.\nBuilding Costs: {}\nBuilding Emissions: {},\nPower Generation {}",
                    self.price(shipping_option).to_formatted_string(),
                    self.build_emissions(shipping_option).to_formatted_string(),
                    self.max_power(None).to_formatted_string()
                )
            }
        }
    }
}

impl Display for Building {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Building::Batteries { .. } => write!(f, "Batteries"),
            Building::CoalPlant => write!(f, "Coal Plant"),
            Building::GasPlant => write!(f, "Gas Plant"),
            Building::SolarArray => write!(f, "Solar Array"),
            Building::WindTurbines => write!(f, "Wind Turbines"),
        }
    }
}
