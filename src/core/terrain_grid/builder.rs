use bevy::prelude::*;
use grid::Grid;
use noisy_bevy::fbm_simplex_2d_seeded;

use crate::core::terrain_grid::terrain::{GameTerrain, GridItem, TerrainTile, TerrainType};

#[derive(Copy, Clone)]
pub struct TerrainTypeBuilder {
    pub terrain_type: TerrainType,
    pub maximum_height: f32,
}

pub struct NoiseInfo {
    pub lacunarity: f32,
    pub persistance: f32,
    pub octaves: usize,
    pub noise_scale: f32,
    pub seed: f32,
}

pub struct GameTerrainBuilder {
    pub terrain_type_builders: Vec<TerrainTypeBuilder>,
    pub default_type: TerrainType,
    pub size: (usize, usize),
    pub enabled_size: (usize, usize),
    pub tile_size: f32,
    pub height_map: fn(f32) -> f32,
    pub noise_info: NoiseInfo,
}

impl GameTerrainBuilder {
    pub fn build(&mut self) -> GameTerrain {
        // Sort (ascending)
        self.terrain_type_builders
            .sort_by(|a, b| a.maximum_height.partial_cmp(&b.maximum_height).unwrap());
        let mut grid: Grid<GridItem> = Grid::new(self.size.0, self.size.1);
        for x in 0..self.size.0 {
            for y in 0..self.size.1 {
                let pos = Vec2::new(x as f32 * self.tile_size, y as f32 * self.tile_size);
                let height = fbm_simplex_2d_seeded(
                    pos * self.noise_info.noise_scale,
                    self.noise_info.octaves,
                    self.noise_info.lacunarity,
                    self.noise_info.persistance,
                    self.noise_info.seed,
                );
                let terrain_type = self.terrain_type_from_height(height);
                let grid_item = GridItem {
                    building: None,
                    building_entity: None,
                    terrain_tile: TerrainTile {
                        height: (self.height_map)(height),
                        terrain_type,
                        enabled: x < self.enabled_size.0 && y < self.enabled_size.1,
                    },
                };
                *grid.get_mut(x, y).unwrap() = grid_item;
            }
        }
        GameTerrain {
            enabled_size: self.enabled_size,
            tile_size: self.tile_size,
            terrain_grid: grid,
        }
    }

    fn terrain_type_from_height(&self, height: f32) -> TerrainType {
        for builder in &self.terrain_type_builders {
            if height < builder.maximum_height {
                return builder.terrain_type;
            }
        }
        self.default_type
    }
}

impl Default for GameTerrainBuilder {
    fn default() -> Self {
        let terrain_type_builders = vec![
            TerrainTypeBuilder {
                terrain_type: TerrainType::Water,
                maximum_height: -0.3,
            },
            TerrainTypeBuilder {
                terrain_type: TerrainType::Grass,
                maximum_height: 0.5,
            },
        ];

        Self {
            terrain_type_builders,
            default_type: TerrainType::Mountain,
            size: (10, 10),
            enabled_size: (5, 5),
            tile_size: 5.0,
            height_map: |x| {
                let mut x = x * 1.8;
                x = x.max(0.0);
                x *= x.sqrt();
                x
            },
            noise_info: NoiseInfo {
                lacunarity: 2.0,
                persistance: 0.5,
                octaves: 2,
                noise_scale: 0.015,
                seed: 1.49,
            },
        }
    }
}
