use std::ops::{Index, IndexMut};

use bevy::prelude::*;
use grid::Grid;

use crate::assets::TerrainAssets;
use crate::core::buildings::Building;
use crate::core::terrain_grid::builder::GameTerrainBuilder;

pub struct TerrainPlugin;

impl Plugin for TerrainPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<GameTerrain>()
            .add_system(sync_terrain_material.run_if(resource_changed::<GameTerrain>()));
    }
}

#[derive(Copy, Clone, Default, Eq, PartialEq)]
pub enum TerrainType {
    Water,
    #[default]
    Grass,
    Mountain,
}

#[derive(Copy, Clone, Default)]
pub struct TerrainTile {
    pub height: f32,
    pub terrain_type: TerrainType,
    pub enabled: bool,
}

impl TerrainTile {
    pub fn terrain_multiplier_wind(&self) -> f64 {
        match self.terrain_type {
            TerrainType::Water => 1.2,
            TerrainType::Grass => 1.0,
            TerrainType::Mountain => 1.5,
        }
    }
}

#[derive(Resource)]
pub struct GameTerrain {
    pub tile_size: f32,
    pub terrain_grid: Grid<GridItem>,
    pub enabled_size: (usize, usize),
}

impl Default for GameTerrain {
    fn default() -> Self {
        GameTerrainBuilder::default().build()
    }
}

#[derive(Copy, Clone, Default)]
pub struct GridItem {
    pub building: Option<Building>,
    pub building_entity: Option<Entity>,
    pub terrain_tile: TerrainTile,
}

impl GameTerrain {
    pub fn world_to_grid(&self, position: Vec3) -> (usize, usize) {
        let x = (position.x / self.tile_size) as usize;
        let y = (position.z / self.tile_size) as usize;

        (
            x.min(self.terrain_grid.rows() - 1),
            y.min(self.terrain_grid.cols() - 1),
        )
    }

    pub fn grid_to_world(&self, position: (usize, usize)) -> Vec3 {
        let position = position.min(self.terrain_grid.size());

        let height = self[position.1][position.0].terrain_tile.height;
        let mut world_position =
            Vec3::new(position.0 as f32, height, position.1 as f32) * self.tile_size;
        // Get the center of the tile.
        world_position += 0.5 * self.tile_size;

        world_position
    }

    pub fn has_entry(&self, position: (usize, usize)) -> bool {
        self[position.1][position.0].building.is_some()
    }

    pub fn insert(&mut self, position: Vec3, entity: Entity, building: Building) -> Option<()> {
        let (x, y) = self.world_to_grid(position);
        let entry = &mut self[y][x];

        match entry.building {
            None => {
                entry.building = Some(building);
                entry.building_entity = Some(entity);
                Some(())
            }
            Some(_) => None,
        }
    }

    pub fn remove(&mut self, position: Vec3) -> Option<Entity> {
        let (x, y) = self.world_to_grid(position);
        let entry = &mut self[y][x];

        match entry.building_entity {
            None => None,
            Some(e) => {
                let entity_copy = e;
                entry.building_entity = None;
                entry.building = None;
                Some(entity_copy)
            }
        }
    }

    pub fn get_building(&self, position: Vec3) -> Option<Building> {
        let (x, z) = self.world_to_grid(position);
        let entry = &self[z][x];
        entry.building
    }

    pub fn get_entity(&self, position: Vec3) -> Option<Entity> {
        let (x, z) = self.world_to_grid(position);
        let entry = &self[z][x];
        entry.building_entity
    }

    pub fn enable_row(&mut self) {
        let x = self.enabled_size.0;
        for z in 0..self.enabled_size.1 {
            self.terrain_grid[z][x].terrain_tile.enabled = true;
        }
        self.enabled_size.0 += 1;
    }

    pub fn enable_column(&mut self) {
        let z = self.enabled_size.1;
        for x in 0..self.enabled_size.0 {
            self.terrain_grid[z][x].terrain_tile.enabled = true;
        }
        self.enabled_size.1 += 1;
    }

    pub fn is_enabled(&self, pos: (usize, usize)) -> bool {
        self.terrain_grid[pos.1][pos.0].terrain_tile.enabled
    }
}

impl Index<usize> for GameTerrain {
    type Output = [GridItem];

    fn index(&self, index: usize) -> &Self::Output {
        &self.terrain_grid[index]
    }
}

impl IndexMut<usize> for GameTerrain {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        &mut self.terrain_grid[index]
    }
}

#[derive(Component)]
pub struct TerrainObject(pub usize, pub usize);

fn sync_terrain_material(
    terrain: Res<GameTerrain>,
    query: Query<(Entity, &TerrainObject)>,
    terrain_assets: Res<TerrainAssets>,
    mut commands: Commands,
) {
    for (entity, object) in &query {
        let material =
            terrain_assets.get_material(terrain.terrain_grid[object.1][object.0].terrain_tile);
        commands.entity(entity).insert(material);
    }
}
