use std::f64::consts::FRAC_2_PI;

use bevy::prelude::*;
use quantities::currency::{Currency, MILLION_EURO};
use quantities::duration::{Duration, MONTH};
use quantities::energy::{Energy, KILOWATT_HOUR, WATT_HOUR};
use quantities::mass::{Mass, TONNE};
use quantities::power::{Power, MEGAWATT, WATT};
use quantities::prelude::*;

use crate::core::buildings::Building;
use crate::core::months::{LastMonthStatistics, ProductionTargets};
use crate::core::shipping_options::{ShippingOption, ShippingPlugin};
use crate::core::states::GameState;
use crate::core::terrain_grid::terrain::GameTerrain;
use crate::core::time_scale::{IngameTime, TimeScale};
use crate::core::upgrades::UpgradeStage;

pub struct ResourcePlugin;

impl Plugin for ResourcePlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<Resources>()
            .add_plugin(ShippingPlugin)
            .add_system(update_resources.in_set(OnUpdate(GameState::InGame)));
    }
}

#[derive(Copy, Clone, Resource)]
pub struct Resources {
    pub money: Currency,
    pub co2: Mass,
    pub reputation: Reputation,
    pub power: Power,
    pub energy: Energy,
    pub energy_produced: Energy,
}

impl Resources {
    pub fn add(&mut self, building: &Building, shipping_option: &ShippingOption) {
        self.co2 = self.co2 + building.build_emissions(shipping_option);
        self.money = self.money - building.price(shipping_option);
    }

    pub fn destroy(&mut self, building: &Building, shipping_option: &ShippingOption) {
        self.co2 = self.co2 + building.destroy_emissions(shipping_option);
        self.money = self.money - building.destroy_price(shipping_option);
    }

    pub fn update(
        &mut self,
        building: &Building,
        delta_time: Duration,
        upgrade: Option<&UpgradeStage>,
    ) {
        self.co2 = self.co2 + building.emissions(upgrade) * delta_time;
    }

    pub fn charge_battery(
        &mut self,
        building: &mut Building,
        mut power: Power,
        delta_time: Duration,
        upgrade: Option<&UpgradeStage>,
    ) -> Power {
        let charge_power = building.max_power(upgrade);
        let max_energy = building.max_energy(upgrade);

        if let Building::Batteries { energy } = building {
            power = if power > charge_power {
                charge_power
            } else {
                power
            };
            let energy_to_charge = power * delta_time;

            if *energy >= max_energy {
                return 0.0 * WATT;
            }

            if *energy + energy_to_charge <= max_energy {
                self.energy = self.energy + energy_to_charge;
                *energy = *energy + energy_to_charge;

                power
            } else {
                let remaining_energy = max_energy - *energy;

                self.energy = self.energy + remaining_energy;
                *energy = *energy + remaining_energy;

                remaining_energy / delta_time
            }
        } else {
            unreachable!()
        }
    }

    pub fn discharge_battery(
        &mut self,
        building: &mut Building,
        mut power: Power,
        delta_time: Duration,
        upgrade: Option<&UpgradeStage>,
    ) -> Power {
        let discharge_power = building.max_power(upgrade);

        if let Building::Batteries { energy } = building {
            power = if power > discharge_power {
                discharge_power
            } else {
                power
            };
            let energy_to_discharge = power * delta_time;

            if *energy <= 0.0 * WATT_HOUR {
                return 0.0 * WATT;
            }

            if *energy - energy_to_discharge >= 0.0 * WATT_HOUR {
                self.energy = self.energy - energy_to_discharge;
                *energy = *energy - energy_to_discharge;

                power
            } else {
                let remaining_energy = *energy;

                self.energy = self.energy - remaining_energy;
                *energy = *energy - remaining_energy;

                remaining_energy / delta_time
            }
        } else {
            unreachable!()
        }
    }

    pub fn apply_month(&mut self, month: LastMonthStatistics) {
        self.money = self.money + month.money_earned;
        self.co2 = Mass::new(0.0, TONNE);
        self.reputation = (self.reputation + month.reputation_earned).equiv_amount(ONE) * ONE;
        self.energy_produced = Energy::new(0.0, KILOWATT_HOUR);
    }
}

impl Default for Resources {
    fn default() -> Self {
        Resources {
            money: Currency::new(750.0, MILLION_EURO),
            co2: Mass::new(0.0, TONNE),
            reputation: Reputation::new(0.0, ONE),
            power: Power::new(0.0, MEGAWATT),
            energy: Energy::new(0.0, KILOWATT_HOUR),
            energy_produced: Energy::new(0.0, KILOWATT_HOUR),
        }
    }
}

#[quantity]
#[ref_unit(One, "", NONE)]
#[unit(Percent, "%", NONE, 0.01)]
pub struct Reputation;

impl Reputation {
    pub fn get_value(&self) -> Self {
        let x = self.equiv_amount(ONE);

        (FRAC_2_PI * x.atan()) * ONE
    }
}

pub fn update_resources(
    mut buildings_query: Query<(&mut Building, &Transform, Option<&UpgradeStage>)>,
    time: Res<Time>,
    time_scale: Res<TimeScale>,
    ingame_time: Res<IngameTime>,
    light_query: Query<&DirectionalLight>,
    terrain: Res<GameTerrain>,
    production_targets: Res<ProductionTargets>,
    mut resources: ResMut<Resources>,
) {
    let delta_time = time_scale.convert_delta_time(&time);
    let mut batteries = vec![];

    let illuminance = light_query
        .iter()
        .map(|light| light.illuminance)
        .sum::<f32>();

    resources.power = 0.0 * WATT;

    for (building, transform, upgrade) in &mut buildings_query {
        match *building {
            Building::Batteries { .. } => batteries.push((building, upgrade)),
            _ => {
                let (x, z) = terrain.world_to_grid(transform.translation);
                resources.update(&building, delta_time, upgrade);
                resources.power = resources.power
                    + building.power(
                        transform.translation,
                        terrain.terrain_grid[z][x].terrain_tile,
                        ingame_time.0 as f32,
                        illuminance as f64,
                        upgrade,
                    )
            }
        }
    }

    resources.energy_produced = resources.energy_produced + resources.power * delta_time;

    let power_deviation = resources.power - production_targets.power_target(ingame_time.0 * MONTH);

    if power_deviation.amount().is_sign_positive() {
        let mut remaining_power = power_deviation;

        for (mut battery, upgrade) in batteries {
            if remaining_power.amount().is_sign_negative() {
                break;
            }

            let power_charged =
                resources.charge_battery(&mut battery, remaining_power, delta_time, upgrade);
            remaining_power = remaining_power - power_charged;
        }
    } else {
        let mut required_power = power_deviation * -1.0;

        for (mut battery, upgrade) in batteries {
            if required_power.amount().is_sign_negative() {
                break;
            }

            let power_discharged =
                resources.discharge_battery(&mut battery, required_power, delta_time, upgrade);
            resources.power = resources.power + power_discharged;
            required_power = required_power - power_discharged;
        }
    }
}
