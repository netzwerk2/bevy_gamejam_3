use std::time::Duration;

use bevy::prelude::*;
use quantities::currency::{Currency, EURO, MILLION_EURO};
use quantities::duration::{MONTH, SECOND};
use quantities::energy::{Energy, KILOWATT_HOUR};
use quantities::mass::{Mass, KILOTONNE, TONNE};
use quantities::power::{Power, MEGAWATT, WATT};
use quantities::{HasRefUnit, Quantity};

use crate::core::resources::{Reputation, Resources, ONE, PERCENT};
use crate::core::states::GameState;
use crate::core::time_scale::{IngameTime, TimeScale, TimeScalePlugin};
use crate::ui::end_screen::LossReason;

pub struct MonthPlugin;

impl Plugin for MonthPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<ProductionTargets>()
            .init_resource::<LastMonthStatistics>()
            .add_plugin(TimeScalePlugin)
            .init_resource::<MonthTimer>()
            .init_resource::<LossTimer>()
            .init_resource::<FinalMonth>()
            .init_resource::<MonthsUnsuccessful>()
            .add_event::<MonthEndedEvent>()
            .add_systems(
                (tick_months, month_ended, check_power).in_set(OnUpdate(GameState::InGame)),
            );
    }
}

pub const MONTHS: usize = 18;

#[derive(Copy, Clone, Resource)]
pub struct ProductionTargets {
    pub power_targets: [Power; MONTHS],
    pub co2_targets: [Mass; MONTHS],
}

impl Default for ProductionTargets {
    fn default() -> Self {
        Self {
            power_targets: [
                25.0 * MEGAWATT,
                45.0 * MEGAWATT,
                60.0 * MEGAWATT,
                75.0 * MEGAWATT,
                100.0 * MEGAWATT,
                120.0 * MEGAWATT,
                135.0 * MEGAWATT,
                150.0 * MEGAWATT,
                160.0 * MEGAWATT,
                165.0 * MEGAWATT,
                170.0 * MEGAWATT,
                180.0 * MEGAWATT,
                210.0 * MEGAWATT,
                240.0 * MEGAWATT,
                280.0 * MEGAWATT,
                310.0 * MEGAWATT,
                330.0 * MEGAWATT,
                350.0 * MEGAWATT,
            ],
            co2_targets: [
                20.0 * KILOTONNE,
                20.0 * KILOTONNE,
                20.0 * KILOTONNE,
                16.0 * KILOTONNE,
                12.0 * KILOTONNE,
                9.0 * KILOTONNE,
                7.5 * KILOTONNE,
                7.3 * KILOTONNE,
                6.5 * KILOTONNE,
                6.0 * KILOTONNE,
                5.4 * KILOTONNE,
                4.7 * KILOTONNE,
                4.1 * KILOTONNE,
                3.3 * KILOTONNE,
                2.9 * KILOTONNE,
                2.4 * KILOTONNE,
                2.1 * KILOTONNE,
                1.5 * KILOTONNE,
            ],
        }
    }
}

impl ProductionTargets {
    fn time_to_months(time: quantities::duration::Duration) -> f64 {
        time.equiv_amount(MONTH).div_euclid(1.0)
    }

    pub fn co2_target(&self, time: quantities::duration::Duration) -> Mass {
        self.co2_targets[(Self::time_to_months(time) as usize).min(MONTHS - 1)]
    }

    pub fn power_target(&self, time: quantities::duration::Duration) -> Power {
        self.power_targets[(Self::time_to_months(time) as usize).min(MONTHS - 1)]
    }
}

#[derive(Copy, Clone, Resource)]
pub struct LastMonthStatistics {
    pub month_count: u32,
    pub money_earned: Currency,
    pub reputation_earned: Reputation,
    pub reputation_change_percent: Reputation,
    pub co2_emitted: Mass,
    pub co2_target: Mass,
    pub energy: Energy,
    pub power_average: Power,
    pub power_target: Power,
}

impl Default for LastMonthStatistics {
    fn default() -> Self {
        Self {
            month_count: 0,
            money_earned: Currency::new(0.0, EURO),
            reputation_earned: Reputation::new(0.0, ONE),
            reputation_change_percent: Reputation::new(0.0, PERCENT),
            co2_emitted: Mass::new(0.0, TONNE),
            co2_target: Mass::new(0.0, TONNE),
            energy: Energy::new(0.0, KILOWATT_HOUR),
            power_average: Power::new(0.0, WATT),
            power_target: Power::new(0.0, WATT),
        }
    }
}

impl LastMonthStatistics {
    fn smooth_step(x: f64) -> f64 {
        if x < 0.0 {
            0.0
        } else if x > 1.0 {
            1.0
        } else {
            x.powi(3) * (6.0 * x.powi(2) - 15.0 * x + 10.0)
        }
    }

    pub fn update(
        &mut self,
        production_targets: &ProductionTargets,
        resources: &Resources,
        ingame_time: &IngameTime,
    ) {
        let time = ingame_time.0 * MONTH - 1.0 * MONTH;

        let co2_target = production_targets.co2_target(time);
        let co2_deviation = (co2_target - resources.co2) / co2_target;
        // 1.5 KILOTONNE less
        let co2_deviation_goodwill = (co2_target - resources.co2) / (co2_target - 1.5 * KILOTONNE);
        let power_target = production_targets.power_target(time);
        let reputation_before_percent = resources.reputation.get_value();

        self.month_count += 1;
        // lose more
        self.reputation_earned =
            (co2_deviation + (co2_deviation_goodwill).signum().min(0.0) * 0.5) * 3.0 * ONE;
        let reputation_modifier = (resources.reputation + self.reputation_earned)
            .get_value()
            .equiv_amount(ONE);
        self.money_earned = (dbg!(co2_deviation * 100.0)
            + dbg!(
                Self::smooth_step(reputation_modifier.abs())
                    * reputation_modifier.signum()
                    * 1000.0
            )
            + self.month_count as f64 * 10.0 * reputation_modifier.signum().max(0.0))
            * MILLION_EURO;

        let new_reputation_percent = (self.reputation_earned + resources.reputation).get_value();
        self.reputation_change_percent = new_reputation_percent - reputation_before_percent;
        self.co2_emitted = resources.co2;
        self.co2_target = co2_target;
        self.energy = resources.energy;
        self.power_average =
            resources.energy_produced / quantities::duration::Duration::new(1.0, MONTH);
        self.power_target = power_target;
    }
}

#[derive(Deref, DerefMut, Resource)]
pub struct MonthTimer(pub Timer);

impl Default for MonthTimer {
    fn default() -> Self {
        MonthTimer(Timer::new(
            Duration::from_secs_f32((1.0 * MONTH).equiv_amount(SECOND) as f32),
            TimerMode::Repeating,
        ))
    }
}

#[derive(Deref, DerefMut, Resource)]
pub struct LossTimer(pub Timer);

impl Default for LossTimer {
    fn default() -> Self {
        LossTimer(Timer::new(Duration::from_secs_f32(5.0), TimerMode::Once))
    }
}

#[derive(Resource)]
pub struct FinalMonth(pub usize);

impl Default for FinalMonth {
    fn default() -> Self {
        Self(MONTHS)
    }
}

#[derive(Default, Resource)]
pub struct MonthsUnsuccessful(pub usize);

pub struct MonthEndedEvent;

fn tick_months(
    mut month_timer: ResMut<MonthTimer>,
    time: Res<Time>,
    time_scale: Res<TimeScale>,
    mut ingame_time: ResMut<IngameTime>,
    mut month_ended_events: EventWriter<MonthEndedEvent>,
) {
    month_timer.tick(Duration::from_secs_f64(
        time_scale.convert_delta_time(&time).amount(),
    ));

    ingame_time.0 +=
        time_scale.as_f64() * time.delta_seconds_f64() / (1.0 * MONTH).equiv_amount(SECOND);

    if month_timer.just_finished() {
        month_ended_events.send(MonthEndedEvent);
    }
}

fn month_ended(
    mut commands: Commands,
    month_ended_events: EventReader<MonthEndedEvent>,
    production_targets: Res<ProductionTargets>,
    ingame_time: Res<IngameTime>,
    mut months_unsuccessful: ResMut<MonthsUnsuccessful>,
    mut resources: ResMut<Resources>,
    mut next_state: ResMut<NextState<GameState>>,
    mut last_month_statistics: ResMut<LastMonthStatistics>,
    mut loss_timer: ResMut<LossTimer>,
    mut time_scale: ResMut<TimeScale>,
) {
    if !month_ended_events.is_empty() {
        if resources.money.amount().is_sign_negative()
            || resources.reputation.get_value().amount().is_sign_negative()
        {
            months_unsuccessful.0 += 1;
        }

        if last_month_statistics.month_count + 1 == MONTHS as u32 {
            next_state.set(GameState::GameWon);
            return;
        } else if months_unsuccessful.0 >= 2 {
            commands.insert_resource(LossReason::NegativeRep);
            next_state.set(GameState::GameLost);
            return;
        }

        next_state.set(GameState::MonthEnd);
        last_month_statistics.update(&production_targets, &resources, &ingame_time);
        loss_timer.reset();
        resources.apply_month(*last_month_statistics);
        time_scale.toggle_pause();
    }
}

fn check_power(
    mut commands: Commands,
    resources: ResMut<Resources>,
    production_targets: Res<ProductionTargets>,
    time: Res<Time>,
    ingame_time: Res<IngameTime>,
    mut next_state: ResMut<NextState<GameState>>,
    mut loss_timer: ResMut<LossTimer>,
    time_scale: Res<TimeScale>,
) {
    let power_target = production_targets.power_target(ingame_time.0 * MONTH);

    if resources.power < power_target {
        let deviation = power_target - resources.power;
        loss_timer.tick(Duration::from_secs_f64(
            time_scale.as_f64() / 120000.0
                * time.delta_seconds_f64()
                * (deviation / power_target).powf(2.5)
                * 10.0,
        ));
    }

    if loss_timer.just_finished() {
        commands.insert_resource(LossReason::Power);
        next_state.set(GameState::GameLost)
    }
}
