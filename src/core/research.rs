use std::fmt::Formatter;
use std::hash::Hash;
use std::vec;

pub use bevy::prelude::*;
use bevy::utils::HashMap;
use quantities::currency::{Currency, BILLION_EURO};
use quantities::energy::KILOWATT_HOUR;
use quantities::Quantity;
use strum::IntoEnumIterator;
use strum_macros::EnumIter;

use crate::core::buildings::Building;
use crate::core::resources::Resources;
use crate::core::shipping_options::ShippingOption;
use crate::core::terrain_grid::terrain::GameTerrain;

pub struct ResearchPlugin;

impl Plugin for ResearchPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<ResearchTree>()
            .init_resource::<GridUpgrades>();
    }
}

#[derive(Clone)]
pub struct ResearchNode {
    pub requirements: Vec<ResearchItem>,
    pub costs: Currency,
}

#[derive(Copy, Clone, Hash, Eq, PartialEq, EnumIter)]
pub enum ResearchItem {
    Batteries,
    Gas,
    Solar,
    Wind,
    NewRow(usize),
    NewColumn(usize),
}

impl std::fmt::Display for ResearchItem {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            ResearchItem::Batteries => write!(f, "Batteries"),
            ResearchItem::Gas => write!(f, "Gas Plants"),
            ResearchItem::Solar => write!(f, "Solar Arrays"),
            ResearchItem::Wind => write!(f, "Wind Turbines"),
            ResearchItem::NewRow(_) => write!(f, "New Row"),
            ResearchItem::NewColumn(_) => write!(f, "New Column"),
        }
    }
}

#[derive(Resource)]
pub struct ResearchTree {
    pub researched: HashMap<ResearchItem, bool>,
    pub has_requirements: HashMap<ResearchItem, bool>,
    pub nodes: HashMap<ResearchItem, ResearchNode>,
}

impl FromWorld for ResearchTree {
    fn from_world(world: &mut World) -> Self {
        let terrain = world.get_resource::<GameTerrain>().unwrap();
        let max_size = terrain.terrain_grid.size();
        let enabled_size = terrain.enabled_size;
        Self::new(max_size, enabled_size)
    }
}

#[derive(Resource)]
pub struct GridUpgrades {
    pub rows: usize,
    pub columns: usize,
    pub start_rows: usize,
    pub start_columns: usize,
}

impl FromWorld for GridUpgrades {
    fn from_world(world: &mut World) -> Self {
        let grid = world.get_resource::<GameTerrain>().unwrap();

        GridUpgrades {
            rows: 0,
            columns: 0,
            start_rows: grid.enabled_size.0,
            start_columns: grid.enabled_size.1,
        }
    }
}

impl ResearchTree {
    pub fn new(max_size: (usize, usize), enabled_size: (usize, usize)) -> Self {
        let solar_node = ResearchNode {
            requirements: vec![],
            costs: Currency::new(0.75, BILLION_EURO),
        };

        let wind_node = ResearchNode {
            requirements: vec![ResearchItem::Solar],
            costs: Currency::new(2.0, BILLION_EURO),
        };

        let batteries_node = ResearchNode {
            requirements: vec![],
            costs: Currency::new(0.5, BILLION_EURO),
        };

        let gas_node = ResearchNode {
            requirements: vec![],
            costs: Currency::new(0.50, BILLION_EURO),
        };

        let mut nodes = HashMap::new();
        nodes.insert(ResearchItem::Solar, solar_node);
        nodes.insert(ResearchItem::Wind, wind_node);
        nodes.insert(ResearchItem::Batteries, batteries_node);
        nodes.insert(ResearchItem::Gas, gas_node);

        let mut researched = HashMap::new();
        let mut has_requirements = HashMap::new();

        for item in ResearchItem::iter() {
            match item {
                ResearchItem::NewColumn(_) | ResearchItem::NewRow(_) => {}
                _ => {
                    researched.insert(item, false);
                    has_requirements.insert(item, false);
                }
            }
        }

        nodes.insert(
            ResearchItem::NewRow(0),
            ResearchNode {
                requirements: vec![],
                costs: Currency::new(1.0, BILLION_EURO),
            },
        );
        nodes.insert(
            ResearchItem::NewColumn(0),
            ResearchNode {
                requirements: vec![],
                costs: Currency::new(1.0, BILLION_EURO),
            },
        );

        for i in 1..=(max_size.0 - enabled_size.0) {
            nodes.insert(
                ResearchItem::NewRow(i),
                ResearchNode {
                    requirements: vec![ResearchItem::NewRow(i - 1)],
                    costs: Currency::new(2f64.powi(i as i32), BILLION_EURO),
                },
            );
        }
        for i in 1..=(max_size.1 - enabled_size.1) {
            nodes.insert(
                ResearchItem::NewColumn(i),
                ResearchNode {
                    requirements: vec![ResearchItem::NewColumn(i - 1)],
                    costs: Currency::new(2f64.powi(i as i32), BILLION_EURO),
                },
            );
        }

        for i in 0..=(max_size.0 - enabled_size.0) {
            has_requirements.insert(ResearchItem::NewRow(i), false);
            researched.insert(ResearchItem::NewRow(i), false);
        }
        for i in 0..=(max_size.1 - enabled_size.1) {
            has_requirements.insert(ResearchItem::NewColumn(i), false);
            researched.insert(ResearchItem::NewColumn(i), false);
        }

        let mut tree = Self {
            researched,
            has_requirements,
            nodes,
        };

        tree.update_requirements();

        tree
    }

    pub fn update_requirements(&mut self) {
        for (item, satisfied) in self.has_requirements.iter_mut() {
            if *satisfied {
                continue;
            }
            *satisfied = true;
            for requirement in self.nodes.get(item).unwrap().requirements.iter() {
                if !*self.researched.get(requirement).unwrap() {
                    *satisfied = false;
                    break;
                }
            }
        }
    }

    pub fn can_research(&self, item: ResearchItem, resources: Resources) -> bool {
        self.nodes.get(&item).unwrap().costs <= resources.money
            && *self.has_requirements.get(&item).unwrap()
            && !*self.researched.get(&item).unwrap()
    }

    pub fn try_unlock(&mut self, item: ResearchItem, mut resources: &mut Resources) {
        if !self.can_research(item, *resources) {
            return;
        }
        resources.money = resources.money - self.nodes.get(&item).unwrap().costs;
        *self.researched.get_mut(&item).unwrap() = true;
        self.update_requirements();
    }

    pub fn is_unlocked(&self, item: ResearchItem) -> bool {
        *self.researched.get(&item).unwrap()
    }

    pub fn can_build(&self, building: Building) -> bool {
        match building {
            Building::Batteries { .. } => self.is_unlocked(ResearchItem::Batteries),
            Building::CoalPlant => true,
            Building::GasPlant => self.is_unlocked(ResearchItem::Gas),
            Building::SolarArray => self.is_unlocked(ResearchItem::Solar),
            Building::WindTurbines => self.is_unlocked(ResearchItem::Wind),
        }
    }
}

impl ResearchItem {
    pub fn tooltip(&self, research_tree: &ResearchTree) -> String {
        match self {
            ResearchItem::Batteries => {
                format!(
                    "Unlocks Batteries.\nResearch Costs: {}\nBattery stats:\n{}",
                    research_tree.nodes.get(self).unwrap().costs,
                    Building::Batteries {
                        energy: 0.0 * KILOWATT_HOUR
                    }
                    .tool_tip(&ShippingOption::default())
                )
            }
            ResearchItem::Gas => {
                format!(
                    "Unlocks Gas Power.\nResearch Costs: {}, Gas Plant stats:\n{}",
                    research_tree.nodes.get(self).unwrap().costs,
                    Building::GasPlant.tool_tip(&ShippingOption::default())
                )
            }
            ResearchItem::Solar => {
                format!(
                    "Unlocks Solar Arrays.\nResearch Costs: {}, Solar Array stats:\n{}",
                    research_tree.nodes.get(self).unwrap().costs,
                    Building::SolarArray.tool_tip(&ShippingOption::default()),
                )
            }
            ResearchItem::Wind => {
                format!(
                    "Unlocks Wind Turbines, needs Solar Arrays.\nResearch Costs: {}, Wind Turbine stats:\n{}",
                    research_tree.nodes.get(self).unwrap().costs,
                    Building::WindTurbines.tool_tip(&ShippingOption::default()),
                )
            }
            ResearchItem::NewRow(_) => {
                format!(
                    "Adds 1 row to be built on.\nPrice: {}",
                    research_tree.nodes.get(self).unwrap().costs
                )
            }
            ResearchItem::NewColumn(_) => {
                format!(
                    "Adds 1 column to be built on.\nPrice: {}",
                    research_tree.nodes.get(self).unwrap().costs
                )
            }
        }
    }
}
