use bevy::prelude::*;

use crate::assets::BuildingAssets;
use crate::core::buildings::Building;
use crate::core::placement::{
    display_placement_marker, insert_building_material, place_building, PlacementMarker,
};
use crate::core::states::{DespawnInMenu, GameState};
use crate::core::terrain_grid::terrain::GameTerrain;

pub struct BuildSelectionPlugin;

impl Plugin for BuildSelectionPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<BuildSelection>().add_systems(
            (clear_selection, sync_preview_with_selection)
                .chain()
                .in_set(OnUpdate(GameState::InGame))
                .after(place_building)
                .after(insert_building_material)
                .after(display_placement_marker),
        );
    }
}

#[derive(Resource, Default, Eq, PartialEq, Copy, Clone)]
pub enum BuildSelection {
    #[default]
    None,
    Building(Building),
}

impl BuildSelection {
    pub fn is_none(&self) -> bool {
        *self == BuildSelection::None
    }

    pub fn unwrap(&self) -> &Building {
        match self {
            BuildSelection::Building(b) => b,
            _ => panic!("Called Unwrap on Selection None or Destroy"),
        }
    }
}

impl BuildSelection {
    pub fn get_handle(&self, building_assets: &BuildingAssets) -> Option<Handle<Scene>> {
        match self {
            BuildSelection::Building(b) => Some(building_assets.get_by_building(b)),
            BuildSelection::None => None,
        }
    }
}

fn clear_selection(mut selected: ResMut<BuildSelection>, mouse: Res<Input<MouseButton>>) {
    if mouse.just_pressed(MouseButton::Right) {
        *selected = BuildSelection::None
    }
}

fn sync_preview_with_selection(
    mut commands: Commands,
    building_selection: Res<BuildSelection>,
    building_assets: Res<BuildingAssets>,
    marker_query: Query<Entity, With<PlacementMarker>>,
    game_grid: Res<GameTerrain>,
) {
    if !building_selection.is_changed() {
        return;
    }

    for entity in &marker_query {
        commands.entity(entity).despawn_recursive();
    }

    if let Some(handle) = building_selection.get_handle(building_assets.into_inner()) {
        commands
            .spawn(SceneBundle {
                scene: handle,
                transform: Transform::from_scale(Vec3::splat(game_grid.tile_size) * 1.001),
                ..default()
            })
            .insert(PlacementMarker::default())
            .insert(DespawnInMenu);
    }
}
