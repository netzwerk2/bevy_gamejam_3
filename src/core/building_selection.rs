use bevy::prelude::*;
use bevy_mod_raycast::Intersection;

use crate::core::build_selection::BuildSelection;
use crate::core::placement::TerrainRaycastSet;
use crate::core::terrain_grid::terrain::GameTerrain;
use crate::ui::info_windows::current_building::CurrentBuildingWindow;
use crate::ui::info_windows::HoveringAboveWindow;
use crate::ui::ui_plugin::UiSystemSet;

pub struct BuildingSelectionPlugin;

impl Plugin for BuildingSelectionPlugin {
    fn build(&self, app: &mut App) {
        app.add_system(
            select_building
                .run_if(
                    resource_equals(BuildSelection::None)
                        .and_then(resource_equals(HoveringAboveWindow(false))),
                )
                .after(UiSystemSet::InGame),
        );
    }
}

fn select_building(
    intersection_query: Query<&Intersection<TerrainRaycastSet>>,
    mouse_button_input: Res<Input<MouseButton>>,
    game_grid: Res<GameTerrain>,
    mut current_building_window: ResMut<CurrentBuildingWindow>,
) {
    if !mouse_button_input.just_pressed(MouseButton::Left) {
        return;
    };

    current_building_window.entity = None;
    let Ok(intersection) = intersection_query.get_single() else {
        return;
    };

    if let Some(intersection_position) = intersection.position() {
        let entity = game_grid.get_entity(*intersection_position);
        current_building_window.entity = entity;
    }
}
