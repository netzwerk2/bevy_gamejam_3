use bevy::prelude::*;

use crate::core::buildings::Building;
use crate::core::resources::Resources;
use crate::core::shipping_options::ShippingOption;

pub struct UpgradePlugin;

impl Plugin for UpgradePlugin {
    fn build(&self, app: &mut App) {
        app.add_event::<UpgradeEvent>()
            .add_system(handle_upgrade_event);
    }
}

#[derive(Copy, Clone, Component, Eq, PartialEq)]
pub enum UpgradeStage {
    First,
    Second,
    Third,
}

#[derive(Copy, Clone)]
pub struct UpgradeMultiplier {
    pub power: f64,
    pub co2: f64,
}

impl UpgradeStage {
    fn multiplier(&self) -> UpgradeMultiplier {
        match self {
            UpgradeStage::First => UpgradeMultiplier {
                co2: 0.9,
                power: 1.2,
            },
            UpgradeStage::Second => UpgradeMultiplier {
                co2: 0.8,
                power: 1.5,
            },
            UpgradeStage::Third => UpgradeMultiplier {
                co2: 0.7,
                power: 2.0,
            },
        }
    }

    fn costs(&self) -> f64 {
        match self {
            UpgradeStage::First => 0.6,
            UpgradeStage::Second => 0.75,
            UpgradeStage::Third => 0.0,
        }
    }

    fn next(&self) -> UpgradeStage {
        match self {
            UpgradeStage::First => UpgradeStage::Second,
            UpgradeStage::Second => UpgradeStage::Third,
            UpgradeStage::Third => UpgradeStage::Third,
        }
    }

    fn upgrade_number(&self) -> usize {
        match self {
            UpgradeStage::First => 2,
            UpgradeStage::Second => 3,
            UpgradeStage::Third => 4,
        }
    }
}

pub trait Upgrade {
    fn multiplier(&self) -> UpgradeMultiplier;
    fn costs(&self) -> f64;
    fn next(&self) -> UpgradeStage;
    fn upgrade_number(&self) -> usize;
}

impl<U: Upgrade> Upgrade for Option<U> {
    fn multiplier(&self) -> UpgradeMultiplier {
        match self {
            None => UpgradeMultiplier {
                power: 1.0,
                co2: 1.0,
            },
            Some(u) => u.multiplier(),
        }
    }

    fn costs(&self) -> f64 {
        match self {
            None => 0.5,
            Some(u) => u.costs(),
        }
    }

    fn next(&self) -> UpgradeStage {
        match self {
            None => UpgradeStage::First,
            Some(u) => u.next(),
        }
    }

    fn upgrade_number(&self) -> usize {
        match self {
            None => 1,
            Some(u) => u.upgrade_number(),
        }
    }
}

impl Upgrade for UpgradeStage {
    fn multiplier(&self) -> UpgradeMultiplier {
        self.multiplier()
    }

    fn costs(&self) -> f64 {
        self.costs()
    }

    fn next(&self) -> UpgradeStage {
        self.next()
    }

    fn upgrade_number(&self) -> usize {
        self.upgrade_number()
    }
}

impl<U: Upgrade> Upgrade for &U {
    fn multiplier(&self) -> UpgradeMultiplier {
        (*self).multiplier()
    }

    fn costs(&self) -> f64 {
        (*self).costs()
    }

    fn next(&self) -> UpgradeStage {
        (*self).next()
    }

    fn upgrade_number(&self) -> usize {
        (*self).upgrade_number()
    }
}

impl<'a, U: Upgrade> Upgrade for Mut<'a, U> {
    fn multiplier(&self) -> UpgradeMultiplier {
        (**self).multiplier()
    }

    fn costs(&self) -> f64 {
        (**self).costs()
    }

    fn next(&self) -> UpgradeStage {
        (**self).next()
    }

    fn upgrade_number(&self) -> usize {
        (**self).upgrade_number()
    }
}

pub struct UpgradeEvent(pub Entity);

fn handle_upgrade_event(
    mut commands: Commands,
    mut events: EventReader<UpgradeEvent>,
    mut buildings: Query<(Option<&mut UpgradeStage>, &Building)>,
    mut resources: ResMut<Resources>,
) {
    for event in events.iter() {
        let (mut upgrade, building) = buildings.get_mut(event.0).unwrap();
        let costs = upgrade.costs() * building.price(&ShippingOption::default());

        if resources.money < costs {
            continue;
        }

        match upgrade {
            None => {
                commands.entity(event.0).insert(UpgradeStage::First);
            }
            Some(ref mut u) => **u = u.next(),
        }

        resources.money = resources.money - costs;
    }
}
