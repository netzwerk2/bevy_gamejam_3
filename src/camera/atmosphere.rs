use std::f32::consts::TAU;
use std::time::Duration;

use bevy::prelude::*;
#[cfg(not(target_arch = "wasm32"))]
use bevy_atmosphere::prelude::*;
use quantities::duration::{MONTH, SECOND};
use quantities::HasRefUnit;

use crate::core::months::MonthTimer;
use crate::core::states::GameState;

pub struct AdaptiveAtmospherePlugin;

impl Plugin for AdaptiveAtmospherePlugin {
    fn build(&self, app: &mut App) {
        #[cfg(not(target_arch = "wasm32"))]
        app.init_resource::<AtmosphereModel>()
            .add_plugin(AtmospherePlugin);
        app.init_resource::<CycleTimer>()
            .init_resource::<DayLength>()
            .add_system(daylight_cycle.in_set(OnUpdate(GameState::InGame)));
    }
}

#[derive(Component)]
pub struct Sun;

#[derive(Resource)]
pub struct CycleTimer(Timer);

impl Default for CycleTimer {
    fn default() -> Self {
        CycleTimer(Timer::new(
            Duration::from_millis(1000 / 60),
            TimerMode::Repeating,
        ))
    }
}

#[derive(Resource)]
pub struct DayLength(f32);

impl Default for DayLength {
    fn default() -> Self {
        let days_per_month = 1.0;
        let day_length = (1.0 * MONTH).equiv_amount(SECOND) / days_per_month;
        DayLength(day_length as f32)
    }
}

// We can edit the Atmosphere resource and it will be updated automatically
fn daylight_cycle(
    #[cfg(not(target_arch = "wasm32"))] mut atmosphere: AtmosphereMut<Nishita>,
    mut query: Query<(&mut Transform, &mut DirectionalLight), With<Sun>>,
    mut timer: ResMut<CycleTimer>,
    time: Res<Time>,
    month_timer: Res<MonthTimer>,
    day_length: Res<DayLength>,
) {
    let ingame_elapsed = month_timer.elapsed();
    timer.0.tick(time.delta());

    if timer.0.finished() {
        let t = ingame_elapsed.as_secs_f32() / day_length.0 * TAU + 0.2;
        #[cfg(not(target_arch = "wasm32"))]
        {
            atmosphere.sun_position = Vec3::new(0., t.sin(), t.cos());
        }
        if let Ok((mut light_trans, mut directional)) = query.get_single_mut() {
            light_trans.rotation = Quat::from_rotation_x(-t);
            directional.illuminance = t.sin().max(0.0).powf(2.0) * 100_000.0;
        }
    }
}
