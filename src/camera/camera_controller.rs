use bevy::input::mouse::{MouseMotion, MouseWheel};
use bevy::prelude::*;
use bevy_dolly::prelude::*;

#[derive(Component)]
pub struct MainCamera;

#[derive(Resource)]
pub struct CameraSettings {
    pub movement_speed: f32,
    pub yaw_speed: f32,
    pub yaw_speed_mouse: f32,
    pub pitch_speed_mouse: f32,
    pub zoom_speed: f32,
    pub mouse_zoom_speed: f32,
}

impl Default for CameraSettings {
    fn default() -> Self {
        Self {
            movement_speed: 100.0,
            yaw_speed: 60.0,
            yaw_speed_mouse: 0.1,
            pitch_speed_mouse: 0.1,
            zoom_speed: 80.0,
            #[cfg(not(target_arch = "wasm32"))]
            mouse_zoom_speed: 10.0,
            #[cfg(target_arch = "wasm32")]
            mouse_zoom_speed: 0.2,
        }
    }
}

pub fn update_camera(
    mut rig_query: Query<&mut Rig>,
    keys: Res<Input<KeyCode>>,
    camera_settings: Res<CameraSettings>,
    time: Res<Time>,
    mut mouse_wheel: EventReader<MouseWheel>,
    mut mouse_motion: EventReader<MouseMotion>,
    mouse_buttons: Res<Input<MouseButton>>,
) {
    let Ok(mut rig) = rig_query.get_single_mut() else {
        return;
    };

    let dt = time.delta_seconds();

    let mut forward_planar = rig.final_transform.rotation * Vec3::NEG_Z;
    forward_planar.y = 0.0;

    let mut left_planar = rig.final_transform.rotation * Vec3::X;
    left_planar.y = 0.0;

    let position_driver = rig.driver_mut::<Position>();
    let mut movement_vector = Vec3::ZERO;

    if keys.pressed(KeyCode::W) {
        movement_vector += forward_planar;
    }
    if keys.pressed(KeyCode::S) {
        movement_vector -= forward_planar;
    }

    if keys.pressed(KeyCode::D) {
        movement_vector += left_planar;
    }
    if keys.pressed(KeyCode::A) {
        movement_vector -= left_planar;
    }
    position_driver.position +=
        movement_vector.normalize_or_zero() * dt * camera_settings.movement_speed;

    let position = position_driver.position;

    let mut look_at_driver = rig.driver_mut::<LookAt>();
    look_at_driver.target = position;

    let mut yaw_pitch_driver = rig.driver_mut::<YawPitch>();

    let yaw_speed_multiplied = dt * camera_settings.yaw_speed;
    if keys.pressed(KeyCode::Q) {
        let new = yaw_pitch_driver.yaw_degrees - yaw_speed_multiplied;
        yaw_pitch_driver.yaw_degrees = new.rem_euclid(360.0)
    }
    if keys.pressed(KeyCode::E) {
        let new = yaw_pitch_driver.yaw_degrees + yaw_speed_multiplied;
        yaw_pitch_driver.yaw_degrees = new.rem_euclid(360.0)
    }

    if mouse_buttons.pressed(MouseButton::Middle) {
        let mut yaw_delta = 0.0;
        let mut pitch_delta = 0.0;
        for mouse_delta in mouse_motion.iter() {
            yaw_delta += mouse_delta.delta.x;
            pitch_delta += mouse_delta.delta.y;
        }
        yaw_delta *= camera_settings.yaw_speed_mouse;
        pitch_delta *= camera_settings.pitch_speed_mouse;

        let new = yaw_pitch_driver.yaw_degrees - yaw_delta;
        yaw_pitch_driver.yaw_degrees = new.rem_euclid(360.0);

        let new = yaw_pitch_driver.pitch_degrees + pitch_delta;
        yaw_pitch_driver.pitch_degrees = new.clamp(5.0, 80.0);
    }

    let mut arm_driver = rig.driver_mut::<Arm>();
    let zoom_speed_multiplied = dt * camera_settings.zoom_speed;
    if keys.pressed(KeyCode::R) {
        arm_driver.offset.z += zoom_speed_multiplied;
    }
    if keys.pressed(KeyCode::F) {
        arm_driver.offset.z -= zoom_speed_multiplied;
    }

    let mut wheel_delta = 0.0;
    for wheel_event in mouse_wheel.iter() {
        wheel_delta += wheel_event.y
    }
    arm_driver.offset.z += wheel_delta * camera_settings.mouse_zoom_speed;

    arm_driver.offset.z = arm_driver.offset.z.clamp(-500.0, -15.0)
}
