use bevy::prelude::{resource_equals, IntoSystemConfig};
use bevy_dolly::prelude::Dolly;

use crate::camera::atmosphere::AdaptiveAtmospherePlugin;
use crate::camera::camera_controller::{update_camera, CameraSettings, MainCamera};
use crate::core::research::{App, OnUpdate, Plugin};
use crate::core::states::GameState;
use crate::ui::info_windows::HoveringAboveWindow;

pub mod atmosphere;
pub mod camera_controller;

pub struct CameraControlPlugin;

impl Plugin for CameraControlPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugin(AdaptiveAtmospherePlugin);
        app.add_system(Dolly::<MainCamera>::update_active)
            .init_resource::<CameraSettings>()
            .add_system(
                update_camera
                    .run_if(resource_equals(HoveringAboveWindow(false)))
                    .in_set(OnUpdate(GameState::InGame)),
            );
    }
}
