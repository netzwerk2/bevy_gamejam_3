### Aim of the Game

This game takes place in an alternate reality where people actually care about climate change.
You are working at the public utility company of a small town,
and your main task is to provide the town with enough power for the citizens.
But beware! The citizens want you to emit as little CO₂ as possible.
The more CO₂ you emit, the lower your reputation becomes and the less money you will get.
If you do not have enough money, you can not keep up with the growing power demand
and will be replaced by someone, who, contrary to you, does a decent job.

### Controls

| Input                   | Action                   |
|:------------------------|--------------------------|
| Q/E/middle-click + drag | rotate the camera        |
| W/A/S/D                 | move the camera          |
| R/F/mouse wheel         | zoom                     |
| right-click             | clear building selection |
| left-click              | place/Select buildings   |
| space                   | pause/Unpause the game   |
| .                       | increase the time speed  |
| ,                       | decrease the time speed  |
| -                       | reset the time speed     |

### Plot Controls

| Input               | Action              |
|---------------------|---------------------|
| left-click + drag   | panning             |
| mouse wheel         | scroll vertically   |
| mouse wheel + Shift | scroll horizontally |
| mouse wheel + Ctrl  | zooming             |
| right-click + drag  | box zooming         |
| double-click        | reset view          |

### Core Mechanics

The game time is split into months.
Each month has a power target and a maximum CO₂ emission target.
At the end of every month, you get money based on your reputation and your CO₂ emissions.
You gain or lose reputation depending on the amount of CO₂ emitted.
If you emit more CO₂ than you are supposed to, you will lose reputation, otherwise you gain it.

You start with limited technologies which will not allow you to satisfy the CO₂ constraints in the long term.
By researching new technologies (which costs money) you might be able to emit less CO₂ and satisfy your customers.

Generating too little power will make you lose the game.
You will notice a red progress bar appearing in the bottom right corner if you produce less power than is demanded.
The greater your power deviation is the faster you lose.
However, the progress bar will reset each month.

To open windows like the plot window or building options,
simply click the corresponding icons at the bottom left of the screen.
If you do not know which icon corresponds to which window, hover over the icons to see their tooltips.

The game is paused at the beginning.
Place your buildings first and then choose a game speed you like
(either using the buttons at the bottom or the keybindings).

### Buildings

The only way to generate power is to place buildings.
Each building emits CO₂ when built and some also emit a significant amount of CO₂ each month after being built.
Buildings can be placed through the buildings window.
Once you selected a building, simply click on the terrain to place it.

Clicking on an already placed building will bring up a little menu.
There you can choose between destroying or upgrading the building.
Destroying buildings can be useful to meet the CO₂ targets, but keep in mind that destroying buildings also emits CO₂.

Upgrades cost money and in exchange offer more power (for renewable energy sources)
and less CO₂ emissions (for fossil ones).

The previewed building will be green if you are allowed to place it.
If you do not have enough money, if there already is a building,
or if the building can not be placed on this terrain type, the previewed building will be red.

###### Coal Plants

Coal plants are fairly cheap to build considering the amount of power they generate.
However, they emit a lot of CO₂.
Coal plants can be placed anywhere except on water.

###### Gas Plants

Gas plants are basically coal plants, but better.
They generate slightly more power while emitting less CO₂.
Gas plants can be placed anywhere except on water.

###### Solar Arrays

Solar arrays emit a negligible amount of CO₂, but do not generate that much power at day and none at night.
It is recommended to compensate the lack of power generation at night with batteries.
Solar arrays can be placed anywhere except on water.

###### Wind Turbines

Wind turbines, like solar arrays, do not produce a lot of CO₂, however they generate more power than solar arrays.
Their power generation varies, since it is influenced by the wind.
Wind turbines can be placed anywhere, but they produce more power if they are in the water
and even more when they are on mountains.

###### Batteries

Batteries enable you to store surplus energy.
This energy is fed back into the power grid, if too little power is generated.
Batteries can be placed anywhere except on water.

### Plots

Plots help you to plan ahead.
They display e.g. your current power production and the power demand for all future months to come.
This allows you to decide when to build new buildings.

There are five different plots:

- CO₂: Displays the CO₂ emissions as well as the CO₂ emission targets of the past and future.
- Power: Displays the current power production as well as the power demand of the past and future.
- Energy: Displays the currently stored energy as well as the maximum possible stored energy.
- Money: Displays your money.
- Reputation: Displays your reputation.

### Research

Most of the buildings require research before they can be built.
To research a technology, click the corresponding button in the research window.
You can also expand the limited building space you have by researching new rows or columns.
Some technologies, like wind turbines, require other technologies to be researched first.

Researching costs money, so pay attention to not spend too much on researching a technology that you then can not build.

Technologies which can be researched will be marked light green,
whereas technologies which already have been researched will be marked as dark green.
Technologies which can not be researched,
either due to a lack of money or due to dependencies which have not been researched yet, will be marked red.

### Shipping Options

Shipping options affect the overall costs and CO₂ emissions of buildings when placed.
There are four shipping options: bicycles, electric cars, big trucks and standard.
For more information see their corresponding tooltips.