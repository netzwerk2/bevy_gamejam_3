# Climate

- you play as public utility
- you need to generate power
```
fossile fuels (bad) -> renewable energy (good, but problems storing energy) -> batteries             -> lithium (bad)
                                                                            -> hydrogen              -> strong tanks (bad)
                                                                                                     -> (cooling)
                                                                            -> dam                   -> concrete (bad)
                                                                            -> nuclear fission (bad)

```

- start in a small city
- start with 1/2 small coal plants

- city gets bigger --> need for more power
- sustainability gets more into focus of society -> less carbon dioxide emission allowed over time -> less money

- finite space to build but gets bigger the further you progress if you are good


  
- bevy egui plots  

# Energy sources / Buildings

## Research
- costs money
- takes time

## Coal / Gas / ...
- cheap, much energy
- much carbon over time

## Renewable
- solar
- wind
- water

- energy level not constant, not so much energy

## Storages
- stores power over time -> has maximum charge power
- if production < needed -> feeds into power grid, has maximum power 

- pumped-storage plant
- hydrogen, can be transported over pipes?
- battery

## logistic? -> later
- overhead cable...
- pipes? 

# Acquiring resources
- buy them
- multiple shipping options -> more money = less CO2

# Mechanics
- Buildings cost money
- building emits carbon dioxide, resources too
- Carbon dioxide can be emitted over time, e.g. by coal plant
- customer experience
- based on month
  - at the end of month:
    - you get money based on revenue
    - revenue changes based on CO2 emission
    - CO2 for month is reset

# Level design
- hand-made levels
- topology influences build costs and what you can build
- square grid
- animations

# Ending
- too few power generated (mostly because no money)
- ends after x months, if not lost, you won

# Prototype
- Flat map, no terrain
  - but grows
- core mechanics
  - placing buildings ✓
  - months ✓
  - money, CO2, revenue, power ✓
  - research ✓
  - losing ✓
  - resource interface ✓
- minimum amount of buildings
  - coal ✓
  - wind ✓
  - batteries
- extensible (e.g. make enums even if there is only one type yet)

# Order
1. Map
2. Placing Buildings
3. resources

# Dependencies
- map -> building
- building -> resources
- UI
- 3D Models

# Balancing
        Solar : Wind : Coal
Costs     1   :  6   :  30
Build     1   :  10  :  75
Time      0   :  0   :  1
Power     1   :  4   :  20