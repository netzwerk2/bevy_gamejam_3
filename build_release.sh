#!/bin/sh

target="$1"

cwd="$(dirname "$(realpath "$0")")"
project_name="$(basename "$cwd")"

if [ -z "$target" ]; then
    echo "USAGE: build_release.sh TARGET"
    echo "Available targets are: linux, wasm, windows"
    exit 1
fi

cd "$cwd"

case "$target" in
linux)
    rm -rf builds/linux
    mkdir -p builds/linux
    cargo build --release --features bevy/wayland
    cp "target/release/$project_name" builds/linux/
    rm -rf builds/linux/assets
    rsync -Pav assets builds/linux --exclude "*.blend*"
    compressed_file="builds/linux/${project_name}_linux.tar.gz"
    rm -f "$compressed_file"
    ouch c builds/linux/* "$compressed_file"
    ;;
wasm)
    rm -rf builds/wasm
    mkdir -p builds/wasm
    cargo build --profile wasm-release --target wasm32-unknown-unknown
#    wasm-opt -O1 --output optimized.wasm "target/wasm32-unknown-unknown/wasm-release/$project_name.wasm"
#    mv optimized.wasm "target/wasm32-unknown-unknown/wasm-release/$project_name.wasm"
    wasm-bindgen --no-typescript --out-name "$project_name" --out-dir builds/wasm \
        --target web "target/wasm32-unknown-unknown/wasm-release/$project_name.wasm"
    curl -s https://raw.githubusercontent.com/bevyengine/bevy/main/examples/wasm/index.html > builds/wasm/index.html
    sed -i "s%./target/wasm_example.js%./bevy_gamejam_3.js%g" builds/wasm/index.html
    sed -i 's/<script type="module">/<script type="module">\n    document.addEventListener("contextmenu", event => event.preventDefault());\n/' \
        builds/wasm/index.html
    rm -rf builds/wasm/assets
    rsync -Pav assets builds/wasm --exclude "*.blend*"
    compressed_file="builds/wasm/${project_name}_wasm.zip"
    rm -f "builds/wasm/${project_name}_wasm.zip"
    ouch c builds/wasm/* "$compressed_file"
    ;;
windows)
    rm -rf builds/windows
    mkdir -p builds/windows
    cargo build --release --target x86_64-pc-windows-gnu
    cp "target/x86_64-pc-windows-gnu/release/$project_name.exe" builds/windows/
    rm -rf builds/windows/assets
    rsync -Pav assets builds/windows --exclude "*.blend*"
    compressed_file="builds/windows/${project_name}_windows.zip"
    rm -f "$compressed_file"
    ouch c builds/windows/* "$compressed_file"
    ;;
*)
    echo "Unknown target '$target'"
    echo "Available targets are: linux, wasm, windows"
esac
